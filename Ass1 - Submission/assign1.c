#include <stdlib.h>
#include <stdio.h>
#include <math.h>	/* FOR pow, sin, cos etc */
#include <stdbool.h>	/* FOR BOOLEAN */

/* System Specific compile commands */
#if _WIN32
#   include <Windows.h>
#endif
#if __APPLE__
#   include <OpenGL/gl.h>
#   include <OpenGL/glu.h>
#   include <GLUT/glut.h>
#else
#   include <GL/gl.h>
#   include <GL/glu.h>
#   include <GL/glut.h> 
#endif

/* The following struct is used for two different purposes, it is used to pass
 * x, and y coordinates as well as vectors */
typedef struct 
{
	float x;
	float y;
} Vector;

/* Create instance of the tangent variable struct */
Vector vector;				/* Current vector to be calculated */
Vector normalizedVector;	/* Used to store the current normalized vector */
Vector velocity;			/* Current velocity of the frog */


/* Global variable storage, allowing access across multiple functions */
typedef struct 
{
	double t;			/* Elapsed time of current jump */
	double lastT;		/* Stores the last time delta time was calculated */
	double dt;			/* Delta time or the change in time between 
						 * calculations */
	double startTime;	/* Time at which the jump was started */

	double frogStartX, frogStartY; /* Position of the frog at the start of the
									* jump */
	double frogX, frogY;/* Current position of the frog / projectile */

	double flightTime; 	/* Length of time the frog will take between jump and 
						 * landing */

	double rotation;	/* variable used to represent a rotation around the z axis
						 * (this will be in radians) */
	double speed;		/* variable used to represent the initial speed of the 
						 * projectile */
	int noOfSegments; 	/* Used to show the current no of segments representing 
						 * shapes on screen */

	double vectorScale;	/* Scale use for ALL normals and tangents */

	/* FLAGS */
	bool start;			/* Used to start and stop the projectile */
	bool cartesianFlag;	/* Change between Cartesian and Parametric 
						 * calculations */
	bool normalsFlag;	/* Turn normals on and off  */ 
	bool tangentsFlag;	/* Turn tangents on and off */
	bool analyticFlag;	/* Change between analytic and numerical 
						 * calculations */
	bool debugFlag;		/* Turns of debugging commands */
} Global;

/* Create instance of the global variable struct */
Global global;

/* GLOBAL CONSTANTS */
const float GRAVITY = 9.8;


/* ########################## UTILITY FUNCTIONS ############################ */

void drawLine(double x, double y, double a, double b, double red, double green,
				 double blue)
{
	glBegin( GL_LINES );
	glColor3f( red, green, blue );
	glVertex3f( x, y, 0.0 );
	glVertex3f( a, b, 0.0 );
	glEnd();
}

/* Draw the 3D axis to the display, NOTE the z axis is not visible due 
 * to the current viewpoint */
void drawAxes(float length)
{
	/* DRAWS X AXIS - RED */
	drawLine( 0, 0, length, 0, 1, 0, 0);
	/* DRAWS Y AXIS - GREEN */
	drawLine( 0, 0, 0, length, 0, 1, 0);

	/* DRAWS Z AXIS - BLUE */
	/* NOTE drawline function cannot be used, as it doesn't support lines in 
	 * the z axis */
	glBegin( GL_LINES );
	glColor3f( 0, 0, 1 );
	glVertex3f( 0, 0, 0 );
	glVertex3f( 0, 0, length ); 
	glEnd();
}

void normalizeVector(double startX, double startY, double x, double y, 
						bool isCircle )
{
	/* Normalize the vector */
	double magnitude = sqrt(( x * x ) + ( y * y ));
	x = ( x / magnitude ); 
	y = ( y / magnitude );

	/* Scale the vector */
	x = ( x * global.vectorScale );
	y = ( y * global.vectorScale );

	/* Shift the vector to the correct position 
	 * If the vector is in the negative x area of the screen, then the
	 * tangents / normals need to be reversed, EXCEPT for the circles */
	if (( startX > 0 ) || ( isCircle ))
	{
		normalizedVector.x = ( x + startX ); 	  
		normalizedVector.y = ( y + startY );
	}
	else
	{
		normalizedVector.x = ( -x + startX ); 	  
		normalizedVector.y = ( -y + startY );			
	}
}

void stopFrog()
{
	global.frogY = 0;  /* Stopping the frog at y = 0 */
	global.start = false;
	global.frogStartX = global.frogX;
	global.frogStartY = global.frogY;	
	velocity.y = global.speed * sin(global.rotation);

	global.t = 0;
	global.dt = 0;
	global.lastT = -1.0;

	global.vectorScale = 0.05;
}


/* ########################## DRAW FROG #################################### */

void drawCartesianCircle( double circleRadius ) 	 
{
	double x, y;
	int i;

	/* Starting from the leftmost position, we can draw the circle */
	x = -circleRadius;

	/* Draw Circle */
	glBegin( GL_LINE_STRIP );
	glColor3f( 0, 1, 0 );
	for ( i = 0; i <= (global.noOfSegments); i++ )
	{
		y = ( circleRadius * circleRadius ) - (x * x);
		y = sqrt( y );

		if (global.debugFlag)
			printf("Cartes Circle()  i = %d  x = %f, y = %f \n", i, x, y);

		/* Draw and Shift the circle */
		/* Draw top half of the circle */
		if ( i < (global.noOfSegments / 2) )
		{
			glVertex3f((x + global.frogX), (y + global.frogY), 0.0);
			x += 0.1/(global.noOfSegments/2);
		}
		/* Draw the bottom half of the circle */
		else 
		{
			glVertex3f((x + global.frogX), (-y + global.frogY), 0.0);
			x -= 0.1/(global.noOfSegments/2);
		}
	}
	glEnd();

	/* #######################################################################
	 * ONLY HALF THE TANGENTS/NORMALS ARE DRAWING, AND ARE IN THE WRONG 
	 * 								DIRECTION 
	 * #################################################################### */

	/* If tangents or normals need to be drawn */
	if ( global.tangentsFlag || global.normalsFlag )
	{
		/* Starting from the leftmost position, we can draw the circle */
		x = -circleRadius;

		for ( i = 0; i <= (global.noOfSegments); i++ )
		{
			y = ( circleRadius * circleRadius ) - (x * x);
			y = sqrt( y );


			vector.y = ( circleRadius * circleRadius ) - (2 * x);
			vector.y = sqrt( vector.y );


			/* Draw and Shift the circle */
			/* Draw top half of the circle */
			if ( i < (global.noOfSegments / 2) )
			{
				if ( global.tangentsFlag )
				{
					normalizeVector((x + global.frogX), (y + global.frogY),
									 (x + global.frogX), vector.y, true);
					drawLine( (x + global.frogX), (y + global.frogY), 
						normalizedVector.x, normalizedVector.y, 1.0, 1.0, 1.0);
				} 
				if ( global.normalsFlag )
				{
					normalizeVector( (x + global.frogX), (y + global.frogY), 
								-vector.y, (x + global.frogX), true );
					drawLine( (x + global.frogX), (y + global.frogY), 
						normalizedVector.x, normalizedVector.y, 0.0, 1.0, 1.0);
				}

				x += 0.1/(global.noOfSegments/2);
			}
			/* Draw the bottom half of the circle */
			else 
			{
				if ( global.tangentsFlag )
				{
					normalizeVector((x + global.frogX), (-y + global.frogY), 
						(x + global.frogX), -vector.y, true );
					drawLine( (x + global.frogX), (-y + global.frogY), 
						normalizedVector.x, normalizedVector.y, 1.0, 1.0, 1.0);
				} 
				if ( global.normalsFlag )
				{
					normalizeVector( (x + global.frogX), (-y + global.frogY), 
						-vector.y, -(x + global.frogX), true );
					drawLine( (x + global.frogX), (-y + global.frogY), 
						normalizedVector.x, normalizedVector.y, 0.0, 1.0, 1.0);
				}
				x -= 0.1/(global.noOfSegments/2);
			}
		}
	}
}

void drawParametricCircle( double circleRadius )
{
	double j;		/* loop control variable */
	double x, y; 	/* Current circle coordinate */
	double prevX, prevY;	/* Previous circle coordinate */
	double theta = (2 * M_PI / global.noOfSegments);	/* Angular portion
														 * of the circle */
	
	/* Calculate Starting position */
	prevX = global.frogX + circleRadius * (cos(theta * 0.0));
	prevY = global.frogY + circleRadius * (sin(theta * 0.0));

	vector.x = circleRadius * -(sin(theta * 0));
	vector.y = circleRadius * (cos(theta * 0));
	
	for ( j = 1; j <= global.noOfSegments; j++ )
	{
		if ( global.tangentsFlag )
		{
			normalizeVector(prevX, prevY, vector.x, vector.y, true );
			drawLine( prevX, prevY, normalizedVector.x, normalizedVector.y, 
					1.0, 1.0, 1.0);
		} 
		if ( global.normalsFlag )
		{
			normalizeVector(prevX, prevY, vector.y, -vector.x, true );
			drawLine( prevX, prevY, normalizedVector.x, normalizedVector.y, 
					0.0, 1.0, 1.0);
		}

		/* Calculate and draw the next position on the circle */
		x = global.frogX + circleRadius * (cos(theta * j));
		y = global.frogY + circleRadius * (sin(theta * j));
		drawLine( prevX, prevY, x, y, 0, 0, 1);

		if (global.debugFlag)
		{
			printf(" Para Circle()  prevX = %f > x = %f", prevX, x);
			printf(" prevY = %f > y = %f \n", prevY, y); 
		}
		
		/* Assign the current x, y coordinates to the previous variables, 
		 * before starting the next loop */
		prevX = x;
		prevY = y;

		/* Calculate the tangent vectors from the next coordinate */
		vector.x = circleRadius * -(sin(theta * j));
		vector.y = circleRadius * (cos(theta * j));
	}
	
}

void drawCircle() 
{
	double circleRadius = 0.05;

	if ( global.cartesianFlag )
	{
		drawCartesianCircle( circleRadius);
	}
	else
	{
		drawParametricCircle( circleRadius );
	}
}


/* ########################## PROJECTILE FUNCTIONS ######################### */
void drawVelocityVector()
{
	vector.x = global.frogX + cos(global.rotation) * global.speed 
					* global.vectorScale;
	vector.y = global.frogY + sin(global.rotation) * global.speed 
					* global.vectorScale;
	drawLine(global.frogX, global.frogY, vector.x, vector.y, 1.0, 0, 1.0);
}

void drawCartesianParabola()
{
	/* CALCULATE RANGE OF PROJECTILE */
	double range = sin(2 * global.rotation) * global.speed * global.speed 
							/ GRAVITY;
	/* Calculate size of each segment of the line */
	double step = range / global.noOfSegments;	
	
	double x, y;
	int i;

	glBegin( GL_LINE_STRIP );
	glColor3f( 0, 1, 0 );
	for( i = 0; i <= global.noOfSegments; i++)
	{
		x = step * i;

		y = (tan(global.rotation) * x) - GRAVITY / (2 * global.speed 
			* global.speed * cos(global.rotation) * cos(global.rotation)) 
			* x * x;
		
		x += global.frogStartX;

		glVertex3f( x, y, 0 );
	}
	glEnd();

	/* If tangents or normals need to be drawn */
	if ( global.tangentsFlag || global.normalsFlag )
	{
		for( i = 0; i <= global.noOfSegments; i++)
		{
			x = step * i;

			y = (tan(global.rotation) * x) - GRAVITY / (2 * global.speed 
				* global.speed * cos(global.rotation) * cos(global.rotation))
				* x * x;
			
			x += global.frogStartX;


			vector.y = tan(global.rotation) - GRAVITY / (2 * global.speed
			 	* global.speed * cos(global.rotation) * cos(global.rotation)) 
				* 2 * x;

			if ( global.tangentsFlag )
			{
				normalizeVector(x, y, x, vector.y, false );
				drawLine( x, y, normalizedVector.x, normalizedVector.y, 1.0, 
					1.0, 1.0);
			} 
			if ( global.normalsFlag )
			{
				normalizeVector( x, y, -vector.y, x, false );
				drawLine( x, y, normalizedVector.x, normalizedVector.y, 0.0, 
					1.0, 1.0);
			}
		}
	}
}

void drawParametricParabola( )
{
	double dt;
	double flightTime = (2 * global.speed * sin(global.rotation) ) / GRAVITY;
	double thetaTime = flightTime / global.noOfSegments;
	double x, y;
	int i;
	
	glBegin( GL_LINE_STRIP );
	glColor3f( 0, 0, 1 );
	for( i = 0; i <= global.noOfSegments; i++)
	{
		dt = (thetaTime * i);

		x = global.speed * dt * cos(global.rotation) + global.frogStartX;
		y = global.speed * dt * sin(global.rotation) - (0.5 * GRAVITY * dt 
			* dt ) + global.frogStartY;

		glVertex3f( x, y, 0 );
	}
	glEnd();

	/* If tangents or normals need to be drawn */
	if ( global.tangentsFlag || global.normalsFlag )
	{
		for( i = 0; i <= global.noOfSegments; i++)
		{
			dt = (thetaTime * i);

			x = global.speed * dt * cos(global.rotation) + global.frogStartX;
			y = global.speed * dt * sin(global.rotation) - (0.5 * GRAVITY * dt 
				* dt ) + global.frogStartY;

			vector.x = global.speed * cos(global.rotation);
			vector.y = global.speed * sin(global.rotation) - (GRAVITY * dt);

			if ( global.tangentsFlag )
			{
				normalizeVector(x, y, vector.x, vector.y, false );
				drawLine( x, y, normalizedVector.x, normalizedVector.y, 1.0, 
					1.0, 1.0);
			} 
			if ( global.normalsFlag )
			{
				normalizeVector( x, y, -vector.y, vector.x, false );
				drawLine( x, y, normalizedVector.x, normalizedVector.y, 0.0, 
					1.0, 1.0);
			}
		}
	}
}

void drawParabola( )
{
	if (global.cartesianFlag )
	{
		drawCartesianParabola();
	}
	else 
	{
		drawParametricParabola( );
	}
}


/* ########################## ANIMATION FUNCTIONS ########################## */

void calcPositionAnalytical( )
{
	velocity.x = cos(global.rotation) * global.speed;
	velocity.y = sin(global.rotation) * global.speed;

	global.frogX = velocity.x * global.t +  global.frogStartX;
	global.frogY = (velocity.y * global.t) + (0.5 * -GRAVITY * global.t 
		* global.t) + global.frogStartY;
}

void calcPositionNumerical( )
{
	// Position
	global.frogX += global.speed * cos(global.rotation) * global.dt;
	global.frogY += velocity.y * global.dt;

	// Velocity
	velocity.y += -GRAVITY * global.dt;
}

void calcPosition( )
{
	if (global.debugFlag)
	{
		printf("calcPosition()  frogX = %f, frogY = %f \n", global.frogX, 
			global.frogY );
	}
	if (global.frogY < 0)
	{
		stopFrog();
		return;
	}

	if ( global.analyticFlag )
	{
		calcPositionAnalytical();
	}
	else 
	{
		calcPositionNumerical();
	}
}


/* ########################## SYSTEM FUNCTIONS ############################# */

void idle()
{
	/* Calculate total duration of flight before touchdown */
	global.flightTime = (velocity.y * 2 / -GRAVITY);
	if (global.debugFlag)
		printf(" idle()  flightTime = %f \n", global.flightTime);


	if ( global.start ) 
	{
		global.t = glutGet(GLUT_ELAPSED_TIME) / 1000.0 - global.startTime;

		if (global.lastT < 0.0) 
		{
			global.lastT = global.t;
			return;
		}

		global.dt = global.t - global.lastT;
		if (global.debugFlag)
		{
			printf(" idle()  t = %f, dt = %f\n", global.t, global.dt);
		}
		
		calcPosition( );

		global.lastT = global.t;
	}
   
	glutPostRedisplay();	
}

void display()
{
	GLenum err;		/* Used to contain any error codes found */

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	/* Draw the axes to the screen */
	drawAxes(1);

	/* Draw the projectile or frog */
	drawCircle(); 
	drawVelocityVector();
	drawParabola();
	


	glutSwapBuffers();

	// Check for errors		
	while ((err = glGetError()) != GL_NO_ERROR)
	printf("%s\n",gluErrorString(err));
}


void init()
{
	/* In this program these OpenGL calls only need to be done once,
	   but normally they would go elsewhere, e.g. display */

	glMatrixMode(GL_PROJECTION);
	glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);

	/* Set initial Values */
	stopFrog();
	
	global.noOfSegments = 8;
	global.rotation = M_PI/4;
	global.speed = 1;
}


void keyboardSpecial(int key, int x, int y)
{
	switch (key)
	{
		case GLUT_KEY_UP:
			if ( global.noOfSegments < 1048576 )
			{
				global.noOfSegments = global.noOfSegments * 2;
				printf("segments = %d \n", global.noOfSegments);
			}
			break;
		case GLUT_KEY_DOWN:
			if ( global.noOfSegments > 2 )
			{
				global.noOfSegments = global.noOfSegments / 2;
				printf("segments = %d \n", global.noOfSegments);
			}
			break;
	}
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
		case 'a':	/* Increases the angle of the velocity vector */
			global.rotation += M_PI/180; 
			printf("Rotation = %f \n", global.rotation);
			break;
		case 'd':	/* Decreases the angle of the velocity vector */
			global.rotation -= M_PI/180;
			printf("Rotation = %f \n", global.rotation);
			break;

		case 'f':	/* Change between Cartesian and Parametric calculations */
			global.cartesianFlag = !global.cartesianFlag;
			if (global.cartesianFlag)
				printf("Cartesian \n");
			else 
				printf("Parametric \n");
			break;

		case 'i':	/* change between analytic and numerical calculations */
			global.analyticFlag = !global.analyticFlag;
			if (global.analyticFlag)
				printf("Analytic Integration \n");
			else 
				printf("Numerical Integration \n");
			break;

		case 27:	/* Quit the program */
		case 'q':
			exit(EXIT_SUCCESS);
			break;

		case 'n':	/* Turn normals on and off */ 
			global.normalsFlag = !global.normalsFlag;
			break;

		case 'r':	/* Reset the frog back to 0,0  */
			global.frogX = 0.0;
			stopFrog();
			break;

		case 't': 	/* Turn tangents on and off */
			global.tangentsFlag = !global.tangentsFlag;
			break;

		case 's':	/* Decreases the speed of the velocity vector */
			global.speed -= 0.1;
			printf("Speed = %f \n", global.speed);
			break;
		case 'w':	/* Increases the speed of the velocity vector */
			global.speed += 0.1;
			printf("Speed = %f \n", global.speed);
			break;

		case 'z':	/* Activate the debugging outputs */
			global.debugFlag = !global.debugFlag;
			break;

		case ' ':	/* Spacebar Key */
			if (!global.start ) 
			{
				
				global.startTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
				global.start = true;
			}
			break;

		default:
			break;
	}
}


int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutCreateWindow("Assignment 1 Cameron Watt s3589163 - FINAL");

	init();

	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(keyboardSpecial);
	glutIdleFunc(idle);
	glutMainLoop();

	return EXIT_SUCCESS;
}