#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>	/* FOR BOOLEAN */
#include <time.h>		/* Used to initialize the random number generator */
#include <math.h>

/* System Specific compile commands */
#if _WIN32
#   include <Windows.h>
#endif
#if __APPLE__
#   include <OpenGL/gl.h>
#   include <OpenGL/glu.h>
#   include <GLUT/glut.h>
#else
#   include <GL/gl.h>
#   include <GL/glu.h>
#   include <GL/glut.h> 
#endif

// Program uses the Simple OpenGL Image Library for loading textures: http://www.lonesock.net/soil.html
#include "SOIL.h"
#include <string.h>

static GLuint texture;

/* TOTAL Number of hazards created. Half will be cars, the rest will be logs 
 * NOTE this must be a macro, as it defines and array size */
#define NUMBEROFOBJECTS 20

const float GRAVITY = 9.8;

typedef struct 
{
  int frames;
  float frameRate;
  float frameRateInterval;
  float lastFrameRateT;
}FrameRate;
FrameRate frameMonitor;


typedef struct {
    float x, y, z;
} vec3f;
vec3f *logVerts;		// Vertices to Draw a Log
vec3f *frogVerts;
vec3f *rectVerts;
vec3f *frogSplats;


typedef struct 
{
	bool drawAxes;
	bool drawNormals;
	bool wireFrame;			// Switch between wireframe and filled
	bool start;
	bool debugFlag;			// Activate debug information
	bool enableLighting;	// Is the lighting on???
	bool enableTextures;
	bool gameOver; 			// Used to indicate that the game has finished
	bool carMovement;
	bool outOfBoundsMsg;	// Used to display an out of bounds warning to player
	int segments;			// Number of segments used to draw all objects
	bool drawColliders;		// Turns on and off collider drawing
}Options;
Options options;	// Instance of the Options struct

int cubeVerts[24] = { 1,0,2,3,
							4,7,0,3,
							4,7,5,6,
							5,6,1,2,
							0,1,4,5,
							3,2,7,6 };

int wedgeVerts[18] = {  0, 2, 1,
						3, 5, 4,
						0, 3, 2, 5,
						2, 5, 1, 4,
						1, 4, 0, 3 };							

typedef struct 
{
	// Camera Rotation and Zooming Variables
	int lastX;
	int lastY;

	int xRotation;
	int yRotation;

	bool rotate;
	bool zoom;

	float zoomFactor;
} Camera;
Camera camera; // Instance of the Camera struct


typedef struct 	//TODO - FUTURE IMPROVEMENT #### MAKE THESE VALUES RELATE TO EACH OTHER, IE A BOARD CHANGE, WILL set roadstart etc
{
	// Start and finish values in both the x and z ranges
	int start;		
	int finish;

	int noOfRows;
	int noOfColumns;
} Board;
Board board = { -8, 8, 8, 8 };
const float ROADSTART = -6;
const float ROADFINISH = -2;
const float RIVERSTART = 2;
const float RIVERFINISH = 6;
#define NUMOFLANES 8	// Number of lanes for both roads and river
float laneSpeeds [ NUMOFLANES ];


enum ObjType { CAR, LOG };
typedef struct 
{
	enum ObjType objType; // Type of object ie Car, Log
	float x, z; // Current position of the object
	float speed; // Speed of the object
	int colour;	// Array value to sub-into Colours Enum
}hazards;
hazards *objList;	// Array of hazards
const float MINSPEED = 0.03;
const float MAXSPEED = 0.06;
const float LOGRADIUS = 0.25;
const float LOGLENGTH = 1.0;


typedef struct 
{
	// Current Position of the frog
	float x, y, z;
	float speed;
	// Position at the start of the jump
	float startX, startY, startZ;

	// X-Y Angle of the frogs direction IN RADIANS
	float theta;
	// X-Z Angle of the frogs direction IN RADIANS
	float phi;

	float xVel;
	float yVel;	// Current gravity on frog
	float zVel;

	// Sphere variables (Collision Detection)
	float radius;
	float normalsLength; // Used on Logs

	// Height of frog model off the ground plane
	float height;

	// Parabola Variables
	float vectorScale;

	// Number of vertices used to produce the frog
	int numVerts;

	// Current range of the projectile
	float range; 

	// Remaining Lives
	int lives;
	// Number of times river has been crossed
	int score;

	// If the frog is currently on a log
	bool onLog;
	// Reference No of the log the frog is attached to
	int logNo;
}Frog;
Frog frog;	 // Instance of the Frog struct

// Limitation of the frog jumps
float MAXRANGE = 2.5;

// Total number of lives the frog starts with
int MAXIMUMLIVES = 5;

int frogAngles[6];	// Joint angles
enum frogJoint { REARHIP, REARUPPERJOINT, REARLOWERJOINT, FRONTHIP, FRONTKNEE, MOUTH };

typedef struct 
{
	float time;
	float value;
} Keyframe;

typedef struct 
{
	float currTime;
	float duration;

	// Number of keyframes being used.
	int nKeyframes;
} Interpolator;
// NOTE This interpolater is used ONLY to time the 
// interpolation of the following keyframe arrays
Interpolator interpolator = { 0, 1.5, 3 };
Keyframe rearHipAngles[] = {
					0,   -40,
					0.5, -90,
					1,   -40  };
Keyframe rearUpperAngles[] = {
					0,   45,
					0.5, 90,
					1,   45 };
Keyframe rearLowerAngles[] = {
					0,   -45,
					0.5, -120,
					1,   -45 };
Keyframe frontHipAngles[] = {
					0,   30,
					0.5, 40,
					1,   30 };
Keyframe frontKneeAngles[] = {
					0,   120,
					0.5,  40,
					1,   120 };


typedef struct
{
	float t;
	float dt;	// Delta Time
	float startTime;
	float lastT;
}Time;
Time timeV;	// Instance of the Time keeping struct

// Colours contained within the setColour function
// Colours on the right are possible selections for car colours, while 
// Colours on the right are not, as they cause various problems
enum Colours { RED, GREEN, ORANGE, PINK, YELLOW, WHITE, BROWN, LBROWN,  	SILVER, GREY, BLUE, FROGGREEN, BLACK, WATER };

// Material colours
GLfloat dull_ambient[] = { 0.3, 0.3, 0.3, 0 };
GLfloat light_ambient[] = { 0.6, 0.6, 0.6, 1.0 };
GLfloat white[] = { 1.0, 1.0, 1.0, 0.0 };


GLfloat red[] = { 1.0, 0.0, 0.0, 0.0 };
GLfloat green[] = { 0.0, 1.0, 0.0, 0.0 };
GLfloat frogGreen[] = {  0.0, 0.3, 0.0, 0.0 };
GLfloat blue[] = { 0.0, 0.0, 1.0, 0.0 };
GLfloat water[] = { 0.0, 0.0, 1.0, 0.5 };
GLfloat brown[] = { 0.55, 0.27, 0.08, 0.0 };
GLfloat lbrown[] = { 0.8, 0.5, 0.25, 0.0 };
GLfloat yellow[] = { 1.0, 1.0, 0.0, 0.0 };
GLfloat black[] = { 0.0, 0.0, 0.0, 0.0 };
GLfloat grey[] = { 0.25, 0.25, 0.25, 0.0 };
GLfloat silver[] = { 0.4, 0.4, 0.4, 0.0 };

GLuint pictureArray [5];
enum textures { IWOOD, IGRASS, IROAD, IRIVER, IRIVERBED };


/* FUNCTION DECLARATIONS */
void initFrogAngles();
float calcTimeOfFlight();
bool checkLogCollision();


/* ########################## UTILITY FUNCTIONS ############################ */

void setColour( enum Colours colour )
{
	switch( colour )
	{
		case RED:
			glColor3f( 1.0, 0.0, 0.0 );
			break;
		case GREEN:
			glColor3f( 0.0, 1.0, 0.0 );
			break;
		case FROGGREEN:
			glColor3f( 0.0, 0.3, 0.0 );
			break;
		case BLACK:			// Frog Eyes
			glColor3f( 0.0, 0.0, 0.0 );
			break;
		case BLUE:
			glColor3f( 0.0, 0.0, 1.0 );
			break;
		case WATER:
			glColor4f( 0.0, 0.0, 0.5, 0.7 );
			break;
		case BROWN:			// LOGS
			glColor3f( 0.55, 0.27, 0.08 );
			break;
		case LBROWN:		// RIVER BED
			glColor3f( 0.8, 0.5, 0.25 );
			break;
		case ORANGE:		// CARS
			glColor3f( 1.0, 0.5, 0.0 );
			break;
		case GREY:			// ROAD
			glColor3f( 0.25, 0.25, 0.25 );
			break;
		case PINK:			// VELOCITY VECTOR
			glColor3f( 1.0, 0.0, 1.0 );
			break;
		case YELLOW:		// NORMALS + TANGENTS
			glColor3f( 1.0, 1.0, 0.0 );
			break;
		case SILVER:		// Hubcaps
			glColor3f( 0.4, 0.4, 0.4 );
			break;
		case WHITE:			
		default:
			glColor3f( 1.0, 1.0, 1.0 );
			break;
	}
}

void setLineColor( enum Colours colour  )
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	setColour( colour );
}

void setMaterial( GLfloat colour[], GLfloat ambient[] )
{
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, colour );
	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient );
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white );
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50 );
}

void enableDisableTextures( GLfloat matColour[], enum Colours colour, enum textures texture )
{
	if ( options.enableTextures )
	{
		glBindTexture(GL_TEXTURE_2D, pictureArray[texture] );
		setMaterial( white, white );
		setColour( WHITE );
	}
	else
	{
		glBindTexture(GL_TEXTURE_2D, 0 );
		setMaterial( matColour, dull_ambient );
		setColour( colour );
	}
}

void drawAxes(float length)
{
	glPushAttrib(GL_CURRENT_BIT);
	/* DRAWS X AXIS */
	glBegin( GL_LINES );
	glColor3f( 1, 0, 0 );
	glVertex3f( 0, 0, 0 );
	glVertex3f( length, 0, 0 );

	/* DRAWS Y AXIS */
	glColor3f( 0, 1, 0 );
	glVertex3f( 0, 0, 0 );
	glVertex3f( 0, length, 0 );

	/* DRAWS Z AXIS */
	glColor3f( 0, 0, 1 );
	glVertex3f( 0, 0, 0 );
	glVertex3f( 0, 0, length );
	glEnd();
	glPopAttrib();
}

void drawRectangle( float xSize, float ySize, float zSize ) 
{
	int k = 0;
	
	float z = -1.0 * zSize;
	for ( int j = 0; j < 2; j++ )
	{
		rectVerts[k].x = -1 * xSize;
		rectVerts[k].y =  1 * ySize;
		rectVerts[k].z = z;
		k++;

		rectVerts[k].x =  1 * xSize;
		rectVerts[k].y =  1 * ySize;
		rectVerts[k].z = z;
		k++;

		rectVerts[k].x =  1 * xSize;
		rectVerts[k].y = -1 * ySize;
		rectVerts[k].z = z;
		k++;

		rectVerts[k].x = -1 * xSize;
		rectVerts[k].y = -1 * ySize;
		rectVerts[k].z = z;
		k++;
		z = 1.0 * zSize;
	}

	k = 0;
	for ( int j = 0; j <= 6; j++ )
	{
		glBegin(GL_TRIANGLE_STRIP);
		for ( int i = 0; i < 4; i++ ) 
		{
			glVertex3fv((float *) &rectVerts[ cubeVerts[k]] );
			glNormal3fv((float *) &rectVerts[ cubeVerts[k]] );
			k++;
		}
		glEnd();
	}
}

float lerp(float t0, float v0, float t1, float v1, float t)
{
	return ( v0 + (t - t0) * (v1 - v0) / (t1 - t0) );
}

/* ########################## BOARD FUNCTIONS ############################ */
void drawRiverBed( float rowStep, float columnStep )
{
	setColour( BROWN );
	setMaterial( brown, white );	
	
	// River Banks
	glBegin( GL_TRIANGLE_STRIP );
	for ( int i = 0; i < board.noOfColumns + 1; i++ )
	{
		glVertex3f( board.start + ( i * columnStep), 0, RIVERSTART );
		glVertex3f( board.start + ( i * columnStep), -0.5, RIVERSTART * 1.2 );
	}
	glEnd();
	glBegin( GL_TRIANGLE_STRIP );
	for ( int i = 0; i < board.noOfColumns + 1; i++ )
	{
		glVertex3f( board.start + ( i * columnStep), 0, RIVERFINISH );
		glVertex3f( board.start + ( i * columnStep), -0.5, RIVERFINISH * 0.9 );
	}
	glEnd();

	// Bind the texture to be used
	enableDisableTextures( lbrown, LBROWN, IRIVERBED );	

	// River Bed
	glBegin( GL_TRIANGLE_STRIP );
		glTexCoord2f( 0, 0 );
		glVertex3f( board.start, -0.5, RIVERSTART * 1.1 );

		glTexCoord2f( 1, 0 );
		glVertex3f( board.finish, -0.5, RIVERSTART * 1.1 );

		glTexCoord2f( 0, 1 );
		glVertex3f( board.start, -0.5, RIVERFINISH * 0.9 );

		glTexCoord2f( 1, 1 );
		glVertex3f( board.finish, -0.5, RIVERFINISH * 0.9 );
	glEnd();
}

// Draw a single Row for the world plane
void drawGridRow( float currZ, float textColStep, float columnStep, float rowStep )
{
	// Draw the triangle Stip to build a single row of the grid
	glBegin( GL_TRIANGLE_STRIP );

	/* We must increse the noOfRows here, to compensate for the extra column 
	 * drawn below. Each iteration, we will draw the points 0,1 + 0,0 */
	for ( int i = 0; i < board.noOfColumns + 1; i++ )
	{
		glTexCoord2f( 0, textColStep * i );
		glNormal3f( 0, 1.0, 0 );
		glVertex3f( board.start + ( i * columnStep), 0, currZ );			// B-Left

		glTexCoord2f( 1, textColStep * i);
		glNormal3f( 0, 1.0, 0 );
		glVertex3f( board.start + ( i * columnStep), 0, currZ + rowStep );	// T-Left
	}
	glEnd();
}

void setRegion( float nextZ )
{
	// START GRASS
	if ( nextZ <= ROADSTART )
		enableDisableTextures( green, GREEN, IGRASS );	
	// ROAD
	else if ( nextZ <= ROADFINISH )
		enableDisableTextures( grey, GREY, IROAD );
	// GRASS IN MIDDLE
	else if ( nextZ <= RIVERSTART )
		enableDisableTextures( green, GREEN, IGRASS );	
	// RIVER
	else if( nextZ <= RIVERFINISH )
	{
		glBindTexture(GL_TEXTURE_2D, 0 );
		// SET TRANSPARENT WATER
		setColour( WATER );
		setMaterial( blue, white );
	}
	// FINAL GRASS
	else
		enableDisableTextures( green, GREEN, IGRASS );
}

void drawGrid( )
{
	float range = board.finish - board.start;
	float rowStep = range / board.noOfRows;
	float columnStep = range / board.noOfColumns;
	
	float currZ;
	float nextZ;

	float textRowStep = 1.0 / board.noOfRows;
	float textColStep = 1.0 / board.noOfColumns;

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	drawRiverBed( rowStep, columnStep );

	for ( int j = 0; j < board.noOfRows; j++ )
	{
		currZ = ( board.start + (j * rowStep) );
		nextZ = currZ + rowStep;

		setRegion( nextZ );

		// Draw the current row, with the flags set above
		drawGridRow( currZ, textColStep, columnStep, rowStep );
	}
	// Disable texture
	glBindTexture(GL_TEXTURE_2D, 0 );

	if ( options.drawNormals )
	{
		// DRAW NORMALS
		for ( int j = 0; j < board.noOfColumns + 1; j++ )
		{
			// Draw the Normals for the above row
			for ( int i = 0; i < board.noOfRows + 1; i++ )
			{
				glBegin( GL_LINES );
				setColour( YELLOW );
				glVertex3f( board.start + ( i * columnStep), 0,   board.start + (j * rowStep) );
				glVertex3f( board.start + ( i * columnStep), 0.5, board.start + (j * rowStep) );
				glEnd();
			}
		}
	}
}

/* ########################## HAZARD FUNCTIONS ############################ */
void initializeLaneSpeeds()
{
	float random;
	float range = MAXSPEED - MINSPEED;
	for ( int lane = 0; lane < ( sizeof(laneSpeeds) / sizeof(float)); lane++ )
	{
		// SET THE STARTING SPEED
		laneSpeeds[lane] = ( (float)rand() )/RAND_MAX * range + MINSPEED;

		// Generate random number between 0 and 2
		random = ((float)rand() )/RAND_MAX * 2;
		if ( random > 1 )
			laneSpeeds[lane] *= -1; // Set Direction to be reversed
	}
}

void createHazards()
{
	// Generic variable used to store the current range
	float range;
	int random;

	// NUMBER OF VALID COLOURS IN COLOURS ENUM
	int colourRange = 8;

	initializeLaneSpeeds();

	// Create each object and add them to the objList List
	for ( int i = 0; i < NUMBEROFOBJECTS; i++ )
	{
		if ( i < ( (int) (0.5* NUMBEROFOBJECTS)) )
		{	
			objList[i].objType = LOG;

			// Calculate the objects lane and speeds
			range = NUMOFLANES / 2;
			//  This will use the first half of the array
			random = (( (float)rand() ) / RAND_MAX * range);

			// Calculate the lane positions
			range = RIVERFINISH - RIVERSTART;
			objList[i].z = RIVERSTART + (2 * LOGRADIUS) + range / (NUMOFLANES / 2) * random;
		}
		else	
		{			
			objList[i].objType = CAR;

			// Pick a random car Colour
			objList[i].colour = (( (float)rand() ) / RAND_MAX * colourRange - 1) / 1;

			// Calculate the objects lane and speeds
			range = NUMOFLANES / 2;
			//  This will use the second half of the array
			random = (( (float)rand() ) / RAND_MAX * range);

			// Calculate the lane positions
			range = (ROADSTART * -1) - (ROADFINISH * -1);
			objList[i].z = ROADSTART + 0.5 + (range / (NUMOFLANES / 2.0)) * random;

			// To ensure the selection of the second half of the array
			random += range;
		}

		// Set the starting speed of the objects
		objList[i].speed = laneSpeeds[ random ];

		// SET THE STARTING COORDINATES
		// Set x coordinate of the object to a value between board.finish and board.start
		range = board.finish - board.start;
		// Generate between (0 and 1) * Range + Add minimum number required
		objList[i].x = ( (float)rand() )/RAND_MAX * range + board.start;

		if ( options.debugFlag )
			printf( "Hazards -> %d - x = %f z = %f speed = %f \n", i, objList[i].x, objList[i].z, objList[i].speed );
	}
}

// Check for collisions of spheres
bool checkCarCollision( hazards car )
{
	if ( frog.y > 0.25 )
		return false;

	float minDist = frog.radius + 0.25;

	float currZDist = frog.z - car.z;
	if ( currZDist < minDist && currZDist > -minDist )
	{
		float currXDist = frog.x - car.x;
		if ( currXDist < minDist && currXDist > -minDist )
		{
			printf("collision detected \n");
			return true;
		}
	}
	return false;
}

// Check for collisions of Logs
bool checkLogCollision( hazards log )
{
	// If the frog is higher than the top of the log
	if ( frog.y - frog.radius > LOGRADIUS )
		return false;

	/* The logs coordinates are marked out as follows in (x, z) format
	 *
	 * ( log.x - LOGLENGTH, log.z + LOGRADIUS )         ( log.x, log.z + LOGRADIUS )
	 *        =============================================================
	 *        |                                                           |
	 *        |                                                           |log.x, log.z
     *        |                                                           |
     *        =============================================================
	 * ( log.x - LOGLENGTH, log.z - LOGRADIUS )         ( log.x, log.z - LOGRADIUS )
	 *
	 * For each collision, we also need to take into account the frogs radius
	*/
	
	// Draw the colliders for the logs
	if ( options.drawColliders )
	{
		// Used to avoid Z-Fighting
		float extra = 0.01;
		vec3f topLeft;
		vec3f topRight;
		vec3f bottomLeft;
		vec3f bottomRight;
		topLeft.x = log.x - LOGLENGTH - extra;
		topLeft.y = 0;
		topLeft.z = log.z + LOGRADIUS + extra;
		
		topRight.x = log.x + extra;
		topRight.y = 0;
		topRight.z = log.z + LOGRADIUS + extra;

		bottomRight.x = log.x + extra;
		bottomRight.y = 0;
		bottomRight.z = log.z - LOGRADIUS - extra;

		bottomLeft.x = log.x - LOGLENGTH - extra;
		bottomLeft.y = 0;
		bottomLeft.z = log.z - LOGRADIUS - extra;


		glBegin( GL_TRIANGLE_STRIP );
		setColour( RED );
			glVertex3fv((float *) &bottomLeft );
			bottomLeft.y += LOGRADIUS;
			glVertex3fv((float *) &bottomLeft );

			glVertex3fv((float *) &bottomRight );
			bottomRight.y += LOGRADIUS;
			glVertex3fv((float *) &bottomRight );

			glVertex3fv((float *) &topRight );
			topRight.y += LOGRADIUS;
			glVertex3fv((float *) &topRight );

			glVertex3fv((float *) &topLeft );
			topLeft.y += LOGRADIUS;
			glVertex3fv((float *) &topLeft );

			// Reset bottomLeft.y to 0 before calling the vertices
			bottomLeft.y -= LOGRADIUS;
			glVertex3fv((float *) &bottomLeft );
			bottomLeft.y += LOGRADIUS;
			glVertex3fv((float *) &bottomLeft );
		glEnd();
	}


	// If the frog is within the correct Z range
	if ( ( frog.z + frog.radius > log.z - LOGRADIUS ) && 
		 ( frog.z - frog.radius < log.z + LOGRADIUS ) )
	{
		// If the frog is within the correct X Range
		if ( ( frog.x + frog.radius > log.x - LOGLENGTH ) &&
			 ( frog.x - frog.radius < log.x ) )
			{
				if ( options.debugFlag )
					printf("LOG collision detected \n");
				return true;
			}
	}
	return false;
}

// Individual object movement
float objMove(float x, float speed, float displacement )
{
	x += speed;

	if ( ( x >= board.finish ) && ( speed > 0 ) )	// x >= LEFTBOUNDARY of Screen
	{
		x = board.start + displacement * timeV.dt; 	// x = RIGHTBOUNDARY  of Screen
	}
	else if ( (x < (board.start + displacement) ) && ( speed < 0 ) )	// x >= RIGHTBOUNDARY of Screen
	{
		x = board.finish; 	// x = LEFTBOUNDARY of Screen
	}
	return x;
}

// Control function to manage ALL object movement
void objMoveALL( )
{
	for ( int i = 0; i < NUMBEROFOBJECTS; i++ )
	{
		if ( objList[i].objType == LOG )
			objList[i].x = objMove( objList[i].x, objList[i].speed, LOGLENGTH );
		else 
			objList[i].x = objMove( objList[i].x, objList[i].speed, 0.5 );
	}
}

void drawWedge( float xSize, float ySize, float zSize )
{
	int k = 0;
	
	float z = -1.0 * zSize;
	for ( int j = 0; j < 2; j++ )
	{
		rectVerts[k].x =  1 * xSize;
		rectVerts[k].y =  1 * ySize;
		rectVerts[k].z = z;
		k++;

		rectVerts[k].x =  1 * xSize;
		rectVerts[k].y = -1 * ySize;
		rectVerts[k].z = z;
		k++;

		rectVerts[k].x = -1 * xSize;
		rectVerts[k].y = -1 * ySize;
		rectVerts[k].z = z;
		k++;
		z = 1.0 * zSize;
	}

	k = 0;
	// Draw Front and Rear Triangles
	for ( int j = 0; j < 2; j++ )
	{
		glBegin(GL_TRIANGLES);
		for ( int i = 0; i < 3; i++ ) 
		{
			glVertex3fv((float *) &rectVerts[ wedgeVerts[k]] );
			k++;
		}
		glEnd();
	}
	// Draw Square Faces
	for ( int j = 0; j < 3; j++ )
	{
		glBegin(GL_TRIANGLE_STRIP);
		for ( int i = 0; i < 4; i++ ) 
		{
			glVertex3fv((float *) &rectVerts[ wedgeVerts[k]] );
			k++;
		}
		glEnd();
	}
}

void drawHubCap()
{
	glPushMatrix();
		glScalef( 0.75, 0.75, 0.05 );
		glutSolidSphere(0.2, 8, 8);
	glPopMatrix();
}

void drawTyre( )
{
	glPushMatrix();
		glColor3f( 0.1, 0.1, 0.1 );
		glScalef( 1, 1, 0.3 );
		glutSolidSphere(0.3, 8, 8);
	glPopMatrix();

	// Draw Hubcaps on BOTH Sides of the tyre
	// glColor3f( 0.6, 0.6, 0.6 );
	setColour( SILVER );
	setMaterial( silver, white );
	glPushMatrix();
		glTranslatef( 0, 0, 0.08 );
		drawHubCap();
	glPopMatrix();
	glPushMatrix();
		glTranslatef( 0, 0, -0.08 );
		drawHubCap();
	glPopMatrix();
	setMaterial( white, white );
}

void objDrawCar( int i )
{
	glPushMatrix();
		// Car Body
		glTranslatef( 0, 0.55, 0 );
		drawRectangle( 0.8, 0.2, 0.5 );

		// Front Grill
		glPushMatrix();
			glTranslatef( 1, 0, 0 );
			glRotatef( 180, 1, 1, 0 );
			drawWedge( 0.2, 0.2, 0.5 );
		glPopMatrix();

		// Rear Spoiler
		glPushMatrix();
			glTranslatef( -1.1, 0, 0 );
			drawWedge( 0.3, 0.2, 0.5 );
		glPopMatrix();

		// Draw Tyres
		// Rear Wheels
		glPushMatrix();
			glTranslatef( 0.6, -0.2, 0.55 );
			drawTyre( );
		glPopMatrix();

		glPushMatrix();
			glTranslatef( 0.6, -0.2, -0.55 );
			drawTyre( );
		glPopMatrix();

		// Front Wheels
		glPushMatrix();
			glTranslatef( -0.7, -0.2, 0.55 );
			drawTyre( );
		glPopMatrix();

		glPushMatrix();
			glTranslatef( -0.7, -0.2, -0.55 );
			drawTyre( );
		glPopMatrix();

		// DRAW GLASS TOP
		glPushAttrib(GL_CURRENT_BIT);
		glColor4f( 0, 0, 1, 0.5 );

			// Car Windows
			glTranslatef( 0, 0.35, 0 );
			glPushMatrix();
				drawRectangle( 0.25, 0.15, 0.5 );
			
				// Front Windscreen
				glTranslatef( -0.5, 0, 0 );
				drawWedge( 0.25, 0.15, 0.5 );
			glPopMatrix();

			glPushMatrix();
				// Rear Windscreen
				glTranslatef( 0.5, 0.0, 0 );
				glRotatef( 180, 0, 1, 0 );
				drawWedge( 0.25, 0.15, 0.5 );
			glPopMatrix();
		glPopAttrib();

		// Car Roof
		setColour( objList[i].colour );
		glTranslatef( 0, 0.17, 0 );
		drawRectangle( 0.25, 0.02, 0.5 );
	glPopMatrix();
}

void initLog()
{
	float r = LOGRADIUS;
	float step_alpha = 2.0 * M_PI / options.segments;

	/* Allocate arrays */
	logVerts = realloc( logVerts, options.segments * sizeof(vec3f) );
	if (!logVerts) 
	{
		printf( "Failed to allocate logVerts array \n" );
		exit(1);  /* And should print a message! */
	}

	// Tube of cylinder
	for ( int j = 0; j < options.segments; j++ ) 
	{
		logVerts[j].y = r * sinf(step_alpha * j );
		logVerts[j].z = r * cosf(step_alpha * j );
	}
}

void objDrawLog()
{
	float textStep = 1.0 / options.segments;
	float iStep = LOGLENGTH / options.segments;

	// Bind the texture to be used
	enableDisableTextures( brown, BROWN, IWOOD );	
	
	int k = 0;
	// Tube of Cylinder
	for (int j = 0; j < options.segments; j++ )
	{
		glBegin( GL_TRIANGLE_STRIP );
		for (int i = 0; i <= options.segments; i++)
		{
			glTexCoord2f( textStep * i, 0 );
			glNormal3f( (-iStep * i), logVerts[j].y, logVerts[j].z );
			glVertex3f( (-iStep * i), logVerts[j].y, logVerts[j].z );
			glTexCoord2f( textStep * i, 1 );
			if ( j == options.segments-1 )
				glVertex3f( (-iStep * i), logVerts[0].y, logVerts[0].z );
			else
				glVertex3f( (-iStep * i), logVerts[j+1].y, logVerts[j+1].z );	
		}
		glEnd();
	}
	
	// Ends of the cylinder
	float x1 = 0;	// LEFT END OF CYLINDER
	for ( int j = 0; j < 2; j++ )
	{
		glBegin(GL_TRIANGLE_STRIP);
		for ( int i = 0; i < options.segments; i++ )
		{
			glVertex3f( x1, 0, 0 );
			glVertex3f( x1, logVerts[i].y, logVerts[i].z );
			glNormal3f( x1, logVerts[j].y, logVerts[j].z );
		}
		glVertex3f( x1, logVerts[0].y, logVerts[0].z );
		glEnd();
		x1 = -LOGLENGTH; // RIGHT END OF CYLINDER
	}

	// Disable texture before normals
	if ( options.enableTextures )
		glBindTexture(GL_TEXTURE_2D, 0 );

	if ( options.drawNormals ) 
	{
		 setLineColor( YELLOW );
		// Outside of the Cylinder
		for (int j = 0; j <= options.segments; j++ )
		{
			glBegin( GL_LINES );
			for ( int i = 0; i <= options.segments; i++ )
			{
				glVertex3f( (-iStep * i), logVerts[j].y, logVerts[j].z );
				glVertex3f( (-iStep * i), logVerts[j].y * frog.normalsLength, logVerts[j].z * frog.normalsLength );
			}
			glEnd();
		}

		// Ends of the cylinder
		x1 = 0 + LOGRADIUS;	// LEFT END OF CYLINDER
		glBegin(GL_LINES);
		for ( int j = 0; j < 2; j++ )
		{
			for ( int i = 0; i < options.segments; i++ )
			{
				glVertex3f( x1, 			logVerts[i].y, logVerts[i].z );
				glVertex3f( x1 - LOGRADIUS, logVerts[i].y, logVerts[i].z );
			}			
			x1 = -LOGLENGTH; // RIGHT END OF CYLINDER
		}
		glEnd();
	}
}

/* Control function to Display ALL objects to screen
 * NOTE I intend to use the Draw functions to test for collisions and return
 * a boolean here */
bool objDisplayAll()
{
	bool collisionDetected = false; 

	for ( int i = 0; i < NUMBEROFOBJECTS; i++ )
	{
		glPushMatrix();
		glTranslatef(objList[i].x, 0, objList[i].z);
		if ( options.drawAxes )
			drawAxes(1);

		if ( objList[i].objType == CAR )  
		{
			setColour( objList[i].colour );
	
				glPushMatrix();
					glScalef( 0.3, 0.3, 0.3 );
					objDrawCar( i );
				glPopMatrix();
			glPopMatrix();
			if ( checkCarCollision( objList[i] ) )
			{
				collisionDetected = true;
			}
		}
		else
		{
				objDrawLog();
			glPopMatrix();
			if ( ( !options.start ) && ( !frog.onLog ) 
				&& ( checkLogCollision( objList[i] ) ) )
			{
				frog.onLog = true;
				frog.logNo = i;
				// Set the frog to be in the middle of the log (on z axis)
				frog.z = objList[i].z - LOGRADIUS;
				frog.y = LOGRADIUS;
				frog.startY = frog.y;
			}
		}  
	}
	return collisionDetected;
}

/* ########################## PROJECTILE FUNCTIONS ######################### */
void initSphere()
{
	// Temp variables to allow shorter code
	int segments = options.segments;	
	float length = frog.normalsLength;

	float r = frog.radius;

	float alpha;
	float beta;

	int k = 0;

	// numVerts =  (segments - ENDS) *  (segments) + 2 end points
	// NOTE +1's are for the <= conditions
	frog.numVerts = (segments - 2 + 1) * (segments + 1) + 2;

	/* Allocate arrays */
	frogVerts = realloc( frogVerts, frog.numVerts * sizeof(vec3f) );
	if (!frogVerts) 
	{
		printf( "Failed to allocate frogVerts array \n" );
		exit(1);  /* And should print a message! */
	}

	frogVerts[k].x = 0;
	frogVerts[k].y = r;	// The vertex at the top of the sphere where x and z = 0
	frogVerts[k].z = 0;
	k++;

	for (int j = 1; j <= segments-1; j++) 
	{
		beta = j / (float)segments * M_PI;
		for (int i = 0; i <= segments; i++)
		{
			alpha = i / (float)segments * 2.0 * M_PI;
			frogVerts[k].x = r * sinf(beta) * cosf(alpha);
			frogVerts[k].y = r * cosf(beta);
			frogVerts[k].z = r * sinf(beta) * sinf(alpha);
			k++;
		}		
	}

	frogVerts[k].x = 0;
	frogVerts[k].y = -r;	// The vertex at the bottom of the sphere where x and z = 0
	frogVerts[k].z = 0;
	k++;
}

void drawSphere()
{
	setMaterial( red, dull_ambient );

	int segments = options.segments;
	float length = frog.normalsLength;

	float nX, nY, nZ;

	int k = 1;
	// Top of sphere
	glBegin(GL_TRIANGLE_STRIP);
	setColour( RED );
	for (int i = 0; i <= segments; i++)
	{	
		glNormal3fv((float *) &frogVerts[ 0 ] );
		glVertex3fv((float *) &frogVerts[ 0 ] );
		glNormal3fv((float *) &frogVerts[ k ] );
		glVertex3fv((float *) &frogVerts[ k ] );
		k++;
	}
	glEnd();

	// Middle of Sphere
	k = 1;
	for (int j = 1; j <= segments-2; j++)
	{
		glBegin(GL_TRIANGLE_STRIP);
		for (int i = 0; i <= segments; i++)
		{
			glNormal3fv((float *) &frogVerts[ k ] );
			glVertex3fv((float *) &frogVerts[ k ] );
			glNormal3fv((float *) &frogVerts[ k+segments+1 ] );
			glVertex3fv((float *) &frogVerts[ k+segments+1 ] );
			k++;
		}
		glEnd();
	}

	// Bottom of sphere
	glBegin(GL_TRIANGLE_STRIP);
	for (int i = 0; i <= segments; i++)
	{	
		glNormal3fv((float *) &frogVerts[ frog.numVerts - 1 ] );
		glVertex3fv((float *) &frogVerts[ frog.numVerts - 1 ] );
		glNormal3fv((float *) &frogVerts[ k ] );
		glVertex3fv((float *) &frogVerts[ k ] );
		k++;
	}
	glEnd();

	if ( options.drawNormals )
	{
		glBegin(GL_LINES);
		setColour( YELLOW );
		for (int i = 0; i < frog.numVerts; i++) 
		{
			nX = frogVerts[ i ].x * frog.normalsLength;
			nY = frogVerts[ i ].y * frog.normalsLength;
			nZ = frogVerts[ i ].z * frog.normalsLength;

			glVertex3fv((float *) &frogVerts[ i ] );
			glVertex3f( nX, nY, nZ );
		}
		glEnd();
	}
}

void drawFoot()
{
	glPushMatrix();
		// Foot
		glTranslatef( 0.5, 0, 0 );

		// Toes
		glPushMatrix(); // INNER TOE
			glRotatef( 15, 0, 1, 0 );
			glTranslatef( 0.25, 0, -0.2 );
			drawRectangle( 0.25, 0.1, 0.1 );
		glPopMatrix();

		glPushMatrix(); // MIDDLE TOE
			glTranslatef( 0.25, 0, 0 );
			drawRectangle( 0.25, 0.1, 0.1 );
		glPopMatrix();

		glPushMatrix(); // OUTER TOE
			glRotatef( -15, 0, 1, 0 );
			glTranslatef( 0.25, 0, 0.2 );
			drawRectangle( 0.25, 0.1, 0.1 );
		glPopMatrix();
	glPopMatrix();
}

/*
 * NOTE - Joint angles are incremented by the angle of the frog, to give
 * the illusion that the frog is rising off the ground when the angle of flight
 * increases, but this is disabled when the frog jumps
 */
void drawFrontLeg( bool animate )
{
	glPushMatrix();
		// Upper Leg
		if ( !animate )
			glRotatef( 30, 0, 0, 1 );
		else if ( options.start )
			glRotatef( frogAngles[ FRONTHIP ], 0, 0, 1 );
		else 
			glRotatef( frogAngles[ FRONTHIP ] + (frog.theta*15), 0, 0, 1 );
		glTranslatef( -0.5, 0, 0 );
		drawRectangle( 0.5, 0.2, 0.2 );

		// Knee
		glTranslatef( -0.5, 0, 0 );
		if (options.drawAxes)
			drawAxes(1);
		if ( !animate )
			glRotatef( 120, 0, 0, 1 );
		else if ( options.start )
			glRotatef( frogAngles[ FRONTKNEE ], 0, 0, 1 );
		else
			glRotatef( frogAngles[ FRONTKNEE ] - (frog.theta*60), 0, 0, 1 );
		
		// Lower Leg
		setColour( FROGGREEN );
		glTranslatef( -0.5, 0, 0 );
		drawRectangle( 0.5, 0.2, 0.2 );
		glRotatef( 180, 0, 0, 1 );
		drawFoot();
	glPopMatrix();
}

void drawRearLeg( bool animate )
{
	glPushMatrix();
		// Upper Leg
		if ( !animate )
			glRotatef( -40, 0, 0, 1 );
		else if ( options.start )
			glRotatef( frogAngles[ REARHIP ], 0, 0, 1 );
		else 
			glRotatef( frogAngles[ REARHIP ] - (frog.theta*30), 0, 0, 1 );
		setColour( FROGGREEN );
		glTranslatef( 0.5, 0, 0 );
		drawRectangle( 0.5, 0.2, 0.2 );

		// RearUpperJoint
		glTranslatef( 0.5, 0, 0 );
		if (options.drawAxes)
			drawAxes(1);
		if ( !animate )
			glRotatef( 45, 0, 0, 1 );
		else if ( options.start )
			glRotatef( frogAngles[ REARUPPERJOINT ], 0, 0, 1 );
		else
			glRotatef( frogAngles[ REARUPPERJOINT ] + (frog.theta*15), 0, 0, 1 );

		// Mid Leg
		setColour( FROGGREEN );
		glTranslatef( -0.5, 0, 0 );
		drawRectangle( 0.5, 0.2, 0.2 );
	
		// RearLowerJoint
		glTranslatef( -0.5, 0, 0 );
		if (options.drawAxes)
		 	drawAxes(1);
		if ( !animate )
			glRotatef( -45, 0, 0, 1 );
		else
			glRotatef( frogAngles[ REARLOWERJOINT ], 0, 0, 1 );

		// Lower Leg
		setColour( FROGGREEN );
		glTranslatef( 0.5, 0, 0 );
		drawRectangle( 0.5, 0.2, 0.2 );
		drawFoot();
	glPopMatrix();
}

void drawEye()
{
	glPushAttrib(GL_CURRENT_BIT);
		setColour( WHITE );
		drawRectangle( 0.08, 0.4, 0.35 );
		glTranslatef( 0.09, 0, 0 );
		setColour( BLACK );
		drawRectangle( 0.01, 0.25, 0.2 );
	glPopAttrib();
}

void drawFrogModel( bool animate )
{
		glPushMatrix();
		setColour( FROGGREEN );
		setMaterial( frogGreen, white );
			// Torso 
			glTranslatef( 1.2, 0, 0 );
			drawRectangle( 1.2, 1, 1 );

			glPushMatrix();
				// Neck
				glTranslatef( 1.2, 0, 0 );

				glPushMatrix();
					// Centre of the Head
					glTranslatef( 0.4, 0, 0 );
						drawRectangle( 0.4, 0.9, 0.8 );

						glPushMatrix();
							// Right Eye
							glTranslatef( 0.48, 0.4, 0.4 );
							drawEye();
						glPopMatrix();

						glPushMatrix();
							// Left Eye
							glTranslatef( 0.48, 0.4, -0.4 );
							drawEye();
						glPopMatrix();

					// Mouth
					setColour( FROGGREEN );
					glTranslatef( 0.9, -0.4, 0 );
					drawRectangle( 0.5, 0.4, 0.8 );
				glPopMatrix();
			glPopMatrix();

			// Front Right Leg
			glPushMatrix();
				// Hip
				glTranslatef( 1.2, -0.7, 1.25 );		
				if (options.drawAxes)
					drawAxes(1);
				setColour( FROGGREEN );
				glRotatef( -10, 1, 1, 0 ); // Rotate leg Inwards slightly
				drawFrontLeg( animate );
			glPopMatrix();

			// Front Left Leg
			glPushMatrix();
				// Hip
				glTranslatef( 1.2, -0.7, -1.25 );
				if (options.drawAxes)
					drawAxes(1);
				setColour( FROGGREEN );
				glRotatef( 10, 1, 1, 0 ); // Rotate leg Inwards slightly
				drawFrontLeg( animate );
			glPopMatrix();

			// Rear Right Leg
			glPushMatrix();
				// Hip
				glTranslatef( -1, 0, 1.15 );
				if (options.drawAxes)
					drawAxes(1);
				setColour( FROGGREEN );
				glRotatef( -30, 1, 1, 0 ); // Rotate leg Outwards
				drawRearLeg( animate );
			glPopMatrix();

			// Rear Left Leg
			glPushMatrix();
				// Hip
				glTranslatef( -1, 0, -1.15 );
				if (options.drawAxes)
					drawAxes(1);
				setColour( FROGGREEN );
				glRotatef( 30, 1, 1, 0 ); // Rotate leg Outwards
				drawRearLeg( animate );
			glPopMatrix();
		glPopMatrix();
	glPopMatrix();
}

void drawFrog()
{
 	glPushMatrix();
		// Translate to frogs position
		glTranslatef( frog.x, frog.y + frog.height, frog.z );
		
		// Show the frogs Collider
		if ( options.drawColliders )	 
			drawSphere();

		glRotatef( (180 / M_PI * -frog.phi), 0, 1, 0 );
		glRotatef( (frog.theta * 30), 0, 0, 1 );
		if ( options.drawAxes )
			drawAxes(1);
		glScalef( 0.08, 0.08, 0.08 );

		drawFrogModel( true );
}

void drawFrogSplattered( float x, float z )
{
	glPushMatrix();
		glTranslatef( x, 0.04, z );

		glScalef( 0.2, 0.01, 0.2 );

		// Rotate the frog to face correct direction
		glRotatef( -90, 0, 1, 0 );
		drawFrogModel( false );
}

void stopFrog()
{
	options.start = false;
	frog.y = 0;
	frog.startX = frog.x;
	frog.startZ = frog.z;
	interpolator.currTime = 0;
	initFrogAngles();
}

// Interpolation function that updates angles of the given arguments
void updateInterpolator( Keyframe keyframes[], int jointNo )
{
	float temp; 

	// interpolator.duration = frog.range;	//################################################################
	// printf("t = %f, range = %f  \n", interpolator.currTime, frog.range );
	// UPDATE THE interpolator.duration TO BE THE FLIGHTTIME OF THE FROG ####################################################################

	if ( interpolator.currTime >= interpolator.duration )
		return;

	for ( int i = 0; i < interpolator.nKeyframes; i++ )
	{
		if ( keyframes[i].time * interpolator.duration >= interpolator.currTime)
		{
			frogAngles[ jointNo ] = lerp( 
				keyframes[i-1].time, 
				keyframes[i-1].value, 
				keyframes[i].time,
				keyframes[i].value,
				interpolator.currTime );
			return;
		}
	}
}

void interpolationCalls()
{
	updateInterpolator( rearHipAngles, REARHIP );
	updateInterpolator( rearUpperAngles, REARUPPERJOINT );
	updateInterpolator( rearLowerAngles, REARLOWERJOINT );
	updateInterpolator( frontHipAngles, FRONTHIP );
	updateInterpolator( frontKneeAngles, FRONTKNEE );
}

void respawnFrog()
{
	frog.x = 0.0;
	frog.z = board.start + frog.radius;
	stopFrog();
}

void killFrog()
{
	frogSplats[ MAXIMUMLIVES - frog.lives ].x = frog.x;
	frogSplats[ MAXIMUMLIVES - frog.lives ].z = frog.z;
	frog.lives--;
	respawnFrog();
}

// Used to check the game rules eg if the frog has crossed the finish line
void checkConditions( )
{
	// If the frog lands in the river 	###################### UNKNOWN ERROR STOPS THIS FROM WORKING #########################################
	// if ( !frog.onLog )
	// {
	// 	if ( ( frog.z > RIVERSTART ) && ( frog.z < RIVERFINISH ) )
	// 		killFrog();
	// }

	// If the frog is out of bounds on the z-axis
	if ( ( frog.z < board.start ) || ( frog.z > board.finish ) )
	{
		killFrog();
		options.outOfBoundsMsg = true;
	}
	// If the frog is out of bounds on the x-axis
	else if ( ( frog.x < board.start ) || ( frog.x > board.finish ) )
	{
		killFrog();
		options.outOfBoundsMsg = true;
	}

	// If the frog has reached the grass on the finish line 
	if ( frog.z > RIVERFINISH )
	{
		frog.score++;
		respawnFrog();
	}
}

void drawVelocityVector()
{
	float vectorScale = 1;

	float targetx = frog.speed * frog.vectorScale * cos(frog.theta) * cos(frog.phi);
	float targety = frog.speed * frog.vectorScale * sin(frog.theta);
	float targetz = frog.speed * frog.vectorScale * cos(frog.theta) * sin(frog.phi);

	glBegin(GL_LINES);
	setColour( PINK );

	glVertex3f( 0, 0, 0);
	glVertex3f( targetx, targety, targetz );
	glEnd();

	if ( options.debugFlag )
		printf( "x = %f, y = %f, z = %f \n", targetx, targety, targetz );
}

void drawParametricParabola()
{
	double dt;
	double flightTime = (2 * frog.speed * sin(frog.theta) ) / GRAVITY;
	double thetaTime = flightTime / options.segments;
	double x, y, z;
	double nX, nY, nZ;
	int i;
	
	glBegin( GL_LINE_STRIP );
	setColour( WHITE );
	for( i = 0; i <= options.segments; i++)
	{
		dt = (thetaTime * i);

		x = frog.speed * dt * cos(frog.theta) * cos(frog.phi);
		y = frog.speed * dt * sin(frog.theta) - (0.5 * GRAVITY * dt * dt );
		z = frog.speed * dt * cos(frog.theta) * sin(frog.phi);

		glVertex3f( x, y, z );
	}
	glEnd();


	// If tangents or normals need to be drawn 
	if ( options.drawNormals )
	{
		glBegin( GL_LINES );
		setColour( YELLOW );
		for( i = 0; i <= options.segments; i++)
		{
			dt = (thetaTime * i);

			x = frog.speed * dt * cos(frog.theta) * cos(frog.phi);
			y = frog.speed * dt * sin(frog.theta) - (0.5 * GRAVITY * dt * dt );
			z = frog.speed * dt * cos(frog.theta) * sin(frog.phi);

			nX = frog.speed * cos(frog.theta) * cos(frog.phi);
			nY = frog.speed * sin(frog.theta) - ( GRAVITY * dt );
			nZ = frog.speed * cos(frog.theta) * sin(frog.phi);
	
			// Normalize the vector 
			double magnitude = sqrt(( nX * nX ) + ( nY * nY ) + ( nZ * nZ ));
			nX = ( nX / magnitude ); 
			nY = ( nY / magnitude );
			nZ = ( nZ / magnitude );

			// Scale the vector and Move into Position
			nX = ( nX * frog.vectorScale ) + x;
			nY = ( nY * frog.vectorScale ) + y;
			nZ = ( nZ * frog.vectorScale ) + z;

			glVertex3f( x, y, z );
			glVertex3f( nX, nY, nZ );
		}
		glEnd();
	}	
}

void calcPosition()	
{
	if ( frog.y < 0 )
	{
		checkConditions( );
		stopFrog();
		return;
	}
	else
	{
		// Position
		frog.x += frog.xVel * timeV.dt;
		frog.y += frog.yVel * timeV.dt;
		frog.z += frog.zVel * timeV.dt;

		// Velocity
		frog.yVel += -GRAVITY * timeV.dt;
	}
}

float calcTimeOfFlight()	// ##################################################################################################
{
	float temp = frog.yVel * 2.0 / GRAVITY;
	printf( "timeOFflight = %f \n", temp );
	return temp;

	// return frog.yVel * 2.0 / GRAVITY;
}

/*
 * Used to track the current range of the frogs jump, to limit the total 
 * distance the frog is allowed to jump
 */
void updateRange( )
{
	frog.range = ( frog.speed * frog.speed ) * sin( 2 * frog.theta ) / GRAVITY;
}

/* ########################## CAMERA FUNCTIONS ############################ */
void mouseMotion(int x, int y)
{
	float zoomScale = 100.0;

	/* Generic camera set variables */
	float dX = x - camera.lastX;
	float dY = y - camera.lastY;
	camera.lastX = x;
	camera.lastY = y;

 	/* Camera Movement */
	if ( camera.rotate )
	{
		camera.xRotation += dX * 0.5;
		camera.yRotation += dY * 0.5;
	}

	/* Camera Scaling */
	if ( camera.zoom )
		camera.zoomFactor -= ( dY / zoomScale );
}

void mouseEvent(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN)
	{
		camera.lastX = x;
		camera.lastY = y;
	} 

	if ( button == GLUT_LEFT_BUTTON )
		camera.rotate = state == GLUT_DOWN;
	else if ( button == GLUT_RIGHT_BUTTON )
		camera.zoom = state == GLUT_DOWN;
}

void cameraMovements() 
{
	// Zoom Functionality
	glTranslatef( 0, 0, camera.zoomFactor);

	// Rotate based upon y-Mouse Movements
	glRotatef( camera.yRotation, 1.0, 0.0, 0.0 );
	// Rotate based upon x-Mouse Movements
	glRotatef( camera.xRotation, 0.0, 1.0, 0.0 );

	// Move the Camera to "follow" the frog
	glTranslatef( -frog.x, -frog.y, -frog.z);

	/* Draw the GLOBAL axes to the screen */
	if (options.drawAxes)
		drawAxes(5);
}

/* ########################## TEXTURE AND LIGHTING FUNCTIONS ############################ */

static GLuint loadTexture(const char *filename)
{
	GLuint tex = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);
	if (!tex)
		return 0;

	glBindTexture(GL_TEXTURE_2D, tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBindTexture(GL_TEXTURE_2D, 0);

	return tex;
}

void lightingFunctions()
{
	GLfloat position [] = {1,1,1,0};

   glEnable( GL_LIGHTING );
   glEnable( GL_LIGHT0 );
   // Lighting turned down as per suggestions from tutor
   // glLightfv(GL_LIGHT0, GL_AMBIENT, dull_ambient );
   // glLightfv(GL_LIGHT0, GL_DIFFUSE, dull_ambient );

   // glLightfv(GL_LIGHT0, GL_AMBIENT, white );
   // glLightfv(GL_LIGHT0, GL_DIFFUSE, white );
   // glLightfv(GL_LIGHT0, GL_SPECULAR, white );

   glLightfv( GL_LIGHT0, GL_POSITION, position );
}

void disableLighting()
{
	glDisable( GL_LIGHTING );
	glDisable( GL_LIGHT0 );
}

/* ########################## SYSTEM FUNCTIONS ############################ */
void displayOSD()
{
	char message[30];
	char buffer[30];
	char *bufp;
	int w, h;

	glPushAttrib(GL_ENABLE_BIT | GL_CURRENT_BIT);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	/* Set up orthographic coordinate system to match the 
	window, i.e. (0,0)-(w,h) */
	w = glutGet(GLUT_WINDOW_WIDTH);
	h = glutGet(GLUT_WINDOW_HEIGHT);
	glOrtho(0.0, w, 0.0, h, -1.0, 1.0);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	/* Frame rate */
	glColor3f(1.0, 1.0, 0.0);
	glRasterPos2i(10, 60);
	snprintf(buffer, sizeof buffer, "framerate (f/s): %6.0f", frameMonitor.frameRate);
	for (bufp = buffer; *bufp; bufp++)
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

	/* Time per frame */
	glColor3f(1.0, 1.0, 0.0);
	glRasterPos2i(10, 40);
	snprintf(buffer, sizeof buffer, "frametime (ms/f): %5.0f", 1.0 / frameMonitor.frameRate * 1000.0);
	for (bufp = buffer; *bufp; bufp++)
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

	/* Lives */
	glColor3f(0.0, 0.5, 0.0);
	glRasterPos2i( (0.5 * w - 60), (h - 20) );
	snprintf(buffer, sizeof buffer, "Lives Remaining: %d", frog.lives);
	for (bufp = buffer; *bufp; bufp++)
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);
	glRasterPos2i( (0.5 * w - 20 ), (h - 40) );
	snprintf(buffer, sizeof buffer, "Score: %d", frog.score);
	for (bufp = buffer; *bufp; bufp++)
	glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);

	/* Out of Bounds Message */
	if ( options.outOfBoundsMsg )
	{
		setColour( RED );
		glRasterPos2i( (0.5 * w - 60), ( 0.5 * h + 40) );
		snprintf(buffer, sizeof buffer, "OUT OF BOUNDS -1 Life");
		for (bufp = buffer; *bufp; bufp++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);
	}

	/* Game Over Message */
	if ( options.gameOver )
	{
		setColour( RED );
		glRasterPos2i( (0.5 * w), ( 0.5 * h + 40) );
		snprintf(buffer, sizeof buffer, "GAME OVER");
		for (bufp = buffer; *bufp; bufp++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);
		glRasterPos2i( (0.5 * w - 60), ( 0.5 * h + 20 ) );
		snprintf(buffer, sizeof buffer, "Please press 'r' to reset");
		for (bufp = buffer; *bufp; bufp++)
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, *bufp);
	}

	/* Pop modelview */
	glPopMatrix();  
	glMatrixMode(GL_PROJECTION);

	/* Pop projection */
	glPopMatrix();  
	glMatrixMode(GL_MODELVIEW);

	/* Pop attributes */
	glPopAttrib();
}

void display()
{
	GLenum err;		/* Used to contain any error codes found */

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_COLOR_MATERIAL);

	//  Draw the world axes at 0,0 with original orientation to the screen 
	if (options.drawAxes)
		drawAxes(1);

	if ( options.enableLighting )
		lightingFunctions();
	else
		disableLighting();

	if ( options.enableTextures )
	{
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		glEnable(GL_TEXTURE_2D);
	}

	cameraMovements();

	// Display previous frogs
	for ( int i = 0; i < MAXIMUMLIVES - frog.lives; i++ )
		drawFrogSplattered( frogSplats[i].x, frogSplats[i].z );

	// Display the objects to the screen
	// Frog death actions
	if ( objDisplayAll() )
		killFrog();

	if ( frog.lives > 0 )
		drawFrog();
	else 
		options.gameOver = true;

	// Draw the ground plane
	drawGrid();

	// Move the vector and parabola to the frogs location
	// NOTE this will only happen if the frog has landed
	glPushMatrix();		
		glTranslatef( frog.startX, frog.startY + frog.height, frog.startZ);
		drawVelocityVector();
		drawParametricParabola();
	glPopMatrix();

	displayOSD();
	glutSwapBuffers();
	
	frameMonitor.frames++;

	// Check for errors		
	while ((err = glGetError()) != GL_NO_ERROR)
	printf("%s\n",gluErrorString(err));
}

void idle()
{
	// Animate the objects
	if (options.carMovement)
		objMoveALL();

	// Update the frogs position relative to the log
	if ( frog.onLog )
	{
		frog.x = objList[ frog.logNo ].x;
		frog.startX = frog.x;
	}

	// Thanks to James and Geoff for the help on the time functions
	static int tLast = -1;
	
	if (tLast < 0)
		tLast = glutGet(GLUT_ELAPSED_TIME);

	timeV.t = glutGet(GLUT_ELAPSED_TIME);
	int dtMs = timeV.t - tLast;
	timeV.dt = (float)dtMs / 1000.0f;
	tLast = timeV.t;

	if ( options.start ) {
		interpolator.currTime += timeV.dt;
		interpolationCalls();
		calcPosition();
	}

	/* Frame rate */
	timeV.dt = timeV.t / 1000.0f - frameMonitor.lastFrameRateT;
	if (timeV.dt > frameMonitor.frameRateInterval) {
		frameMonitor.frameRate = frameMonitor.frames / timeV.dt;
		frameMonitor.lastFrameRateT = timeV.t / 1000.0f;
		frameMonitor.frames = 0;
	}

	glutPostRedisplay();	
}

void reshape (int width, int height) 
{
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();			
	gluPerspective( 75, ((float) width / (float) height), 0.1, 1000 ); 
	
	if ( options.debugFlag )
		printf( "Reshape - width = %d, height = %d  \n", width, height);
}

// Calculate the arrays of points for each of the following objects
void reInitializeObjects()
{
	initSphere();
	initLog();
}

void initFrogAngles()
{
	frogAngles[ REARHIP ] = -40;
	frogAngles[ REARUPPERJOINT ] = 45;
	frogAngles[ REARLOWERJOINT ] = -45;
	frogAngles[ FRONTHIP ] = 30;
	frogAngles[ FRONTKNEE ] = 120;
	frogAngles[ MOUTH ] = 0;
}

void init()
{
	// Seed the random number Generator
	time_t t;	
	srand((unsigned) time(&t));

	glEnable(GL_NORMALIZE);

	// Enable the car movement
	options.carMovement = true;

	camera.zoomFactor = -4;

	camera.xRotation = 180;
	camera.yRotation = 30;

	// Global varible to control the number of segments
	options.segments = 8;

	frog.speed = 3;
	frog.theta = M_PI/4;
	frog.phi = M_PI/2;
	frog.radius = 0.25;
	frog.normalsLength = 1.25;
	frog.vectorScale = 0.25;
	frog.lives = MAXIMUMLIVES;
	frog.height = 0.1;		// Height of the frog model off the ground
	initFrogAngles();

	respawnFrog();

	// Load the textures
	pictureArray [IWOOD]  = loadTexture( "./img/wood.jpg" );
	pictureArray [IGRASS] = loadTexture( "./img/grass.jpg" );
	pictureArray [IROAD]  = loadTexture( "./img/road.jpg" );
	pictureArray [IRIVER] = loadTexture( "./img/river.jpg" );
	pictureArray [IRIVERBED] = loadTexture( "./img/sand.jpg" );

	/* TESTING TEXTURE PATTERNS - MUST LOAD THESE TEXTURES BEFORE TEXTURING
	 * ANY OBJECTS, OTHERWISE THIS OPERATION WILL NOT CHANGE ANYTHING
	 * HENCE WHY THIS MUST BE UNCOMMENTED TO USE. A DEBUG FLAG WILL NOT WORK
	 */
	// for ( int i = 0; i < sizeof(pictureArray)/sizeof(pictureArray[0]); i++ )
	// {
	// 	pictureArray [i] = loadTexture( "GRID.jpg" );
	// }

	// Array to store all of the collision objects
	objList = calloc(NUMBEROFOBJECTS, sizeof(hazards));
	reInitializeObjects();
	createHazards();

	rectVerts = calloc( 8, sizeof(vec3f));
	if (!rectVerts) 
	{
		printf( "Failed to allocate rectVerts array \n" );
		exit(1);  /* And should print a message! */
	}

	frogSplats = calloc( 5, sizeof(vec3f));
	if (!frogSplats) 
	{
		printf( "Failed to allocate frogSplats array \n" );
		exit(1);  /* And should print a message! */
	}
}

void quit()
{
	free(objList);
	free(logVerts);
	free(frogVerts);
	free(rectVerts);
	free(frogSplats);
	printf(" ### THANKS FOR PLAYING ### \n");
	exit(EXIT_SUCCESS);
}

void keyboard(unsigned char key, int x, int y)
{
	// If the input is uppercase, convert to lowercase
	// Credit to   http://www.programmingsimplified.com/c/program/c-program-change-case
	if ( key >= 'A' && key <= 'Z' )
        key = key + 32;

	switch (key)
	{
		case 'o':
			options.drawAxes = !options.drawAxes;
			printf("Toggling axes\n");
			break;
		case 'n':
			options.drawNormals = !options.drawNormals;
			printf("Toggling normals\n");
			break;
		case 'p':
			options.wireFrame = !options.wireFrame;
			printf("Toggling wireframe rendering\n");
			if ( options.wireFrame )
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE );
			else
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL );
			break;
		case 'l':
			if ( options.enableLighting )
			{
				printf("Lighting Disabled \n");
				options.enableLighting = false;
			}
			else
			{
				printf("Lighting Enabled \n");
				options.enableLighting = true;
			}
			break;
		case 't':
		 	options.enableTextures = !options.enableTextures;
		 	printf("Toggling textures\n");
		 	break;	
		case 'w':
			if ( frog.range < MAXRANGE )
			{
				frog.speed += 1.0 * timeV.dt;
				updateRange();
			}
			break;
		case 's':
			if ( frog.speed > 0.1 )
			{
				frog.speed -= 1.0 * timeV.dt;
				updateRange();
			}
			break;
		case 'a':
			if ( ( frog.theta < M_PI/2 ) && ( frog.range < MAXRANGE ) )
			{
				frog.theta += M_PI/4 * timeV.dt;
				updateRange();
			}	
			break;
		case 'd':
			if ( ( frog.theta > 0 ) && ( frog.range < MAXRANGE ) )
			{
				frog.theta -= M_PI/4 * timeV.dt;
				updateRange();
			}
			break;
		case '+':		
			board.noOfRows = board.noOfRows * 2;
			board.noOfColumns = board.noOfColumns * 2;
			options.segments *= 2;
			reInitializeObjects();
			printf("Number of segments %d \n", options.segments );
			break;
		/* NOTE I have used the number of rows on the board as the stop variable
		 * 	  as any less than 8 rows, and the sections (road, river etc) will
		 *		  NOT be display correctly as there are 5 sections which will need
		 *      AT least a row to display each section correctly */
		case '-':		
			if ( board.noOfRows > 8 )
			{
				board.noOfRows = board.noOfRows / 2;
				board.noOfColumns = board.noOfColumns / 2;
				options.segments /= 2;
				reInitializeObjects();
				printf("Number of segments %d \n", options.segments );
			}
			break;
		case 'q':
			quit();
			break;
		case ' ':	/* Spacebar Key - Player MUST stop moving if they lose */
			if ( (!options.start) && (!options.gameOver) ) 
			{				
				// Reset the frog.onLog as the frog is no longer "on the log"
				frog.onLog = false;
				frog.logNo = 0;
				frog.startY = 0;
				options.outOfBoundsMsg = false; 	// Used to clear the out of bounds message
				timeV.startTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
				options.start = true;
				frog.xVel = cos(frog.theta) * cos(frog.phi);
				frog.yVel = sin(frog.theta);
				frog.zVel = cos(frog.theta) * sin(frog.phi);

				// Normalize the vector 
				double magnitude = sqrt(( frog.xVel * frog.xVel ) + ( frog.yVel * frog.yVel ) + ( frog.zVel * frog.zVel ));
				frog.xVel = ( frog.xVel / magnitude ); 
				frog.yVel = ( frog.yVel / magnitude );
				frog.zVel = ( frog.zVel / magnitude );

				// Scale the vector 
				frog.xVel = frog.xVel * frog.speed;
				frog.yVel = frog.yVel * frog.speed;
				frog.zVel = frog.zVel * frog.speed;
			}
		break;

		// #### EXTRA CONTROLS ####
		/* Reset the game */
		case 'r':	
			frog.lives = MAXIMUMLIVES;
			frog.score = 0;
			options.gameOver = false;
			respawnFrog();
			break;

		// Activate the debugging outputs 
		case 'z':	
			options.debugFlag = !options.debugFlag;
			break;

		case 'u':
			options.carMovement = !options.carMovement;
			printf( "Toggling object movement \n" );
			break;

		// Draw Colliders
		case 'c':
			options.drawColliders = !options.drawColliders;
			printf( "Toggling Colliders \n" );
			break;
		default:
			break;
	}
}

void keyboardSpecial(int key, int x, int y)
{
	switch (key)
	{
		case GLUT_KEY_LEFT:
		{
			frog.phi -= M_PI/4 * timeV.dt * 2;
			break;			
		}
		case GLUT_KEY_RIGHT:
		{
			frog.phi += M_PI/4 * timeV.dt * 2;
			break;
		}
	}
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize (700, 700); 
	glutInitWindowPosition (100, 100);
	glutCreateWindow("Assignment 3");

	init();

	// Callbacks
	glutDisplayFunc( display );
	glutMouseFunc( mouseEvent );
	glutMotionFunc( mouseMotion );
	glutKeyboardFunc( keyboard );
	glutSpecialFunc( keyboardSpecial );
	glutReshapeFunc ( reshape );

	glutIdleFunc ( idle );

	glutMainLoop();

	return EXIT_SUCCESS;
}