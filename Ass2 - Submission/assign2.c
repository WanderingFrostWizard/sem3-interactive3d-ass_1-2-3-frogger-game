#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>	/* FOR BOOLEAN */
#include <time.h>		/* Used to initialize the random number generator */
#include <math.h>

/* System Specific compile commands */
#if _WIN32
#   include <Windows.h>
#endif
#if __APPLE__
#   include <OpenGL/gl.h>
#   include <OpenGL/glu.h>
#   include <GLUT/glut.h>
#else
#   include <GL/gl.h>
#   include <GL/glu.h>
#   include <GL/glut.h> 
#endif

// Program uses the Simple OpenGL Image Library for loading textures: http://www.lonesock.net/soil.html
#include "SOIL.h"

static GLuint texture;

/* TOTAL Number of hazards created. Half will be cars, the rest will be logs 
 * NOTE this must be a macro, as it defines and array size */
#define NUMBEROFOBJECTS 20

const float ONERADIAN = M_PI/180;
const float GRAVITY = 9.8;

typedef struct {
    float x, y, z;
} vec3f;
vec3f *logVerts;		// Vertices to Draw a Log
vec3f *carVerts;
vec3f *carNormVerts;
vec3f *frogVerts;

typedef struct 
{
	bool drawAxes;
	bool drawNormals;
	bool wireFrame;			// Switch between wireframe and filled
	bool start;
	bool debugFlag;			// Activate debug information
	bool interactive;			// Switch between gluLookAt or interactive camera controls
	bool enableLighting;				// Is the lighting on???
	bool enableTextures;
}Options;
Options options;	// Instance of the Options struct

int cubeVerts[24] = { 1,0,2,3,
							4,7,0,3,
							4,7,5,6,
							5,6,1,2,
							0,1,4,5,
							3,2,7,6 };

typedef struct 
{
	// Interactive Camera Position
	float xPos;	
	float yPos;

	// Camera Rotation and Zooming Variables
	int lastX;
	int lastY;
	int dX;
	int dY;

	int xRotation;
	int yRotation;

	bool rotate;
	bool zoom;

	float zoomFactorX;
	float zoomFactorY;
} Camera;
Camera camera; // Instance of the Camera struct


typedef struct 	//TODO #### MAKE THESE VALUES RELATE TO EACH OTHER, IE A BOARD CHANGE, WILL set roadstart etc
{
	// Start and finish values in both the x and z ranges
	int start;		
	int finish;

	int noOfRows;
	int noOfColumns;
} Board;
Board board = { -8, 8, 8, 8 };
const float ROADSTART = -6;
const float ROADFINISH = -2;
const float RIVERSTART = 2;
const float RIVERFINISH = 6;


enum ObjType { CAR, LOG };
typedef struct 
{
	enum ObjType objType; // Type of object ie Car, Log
	float x, z; // Current position of the object
	float speed; // Speed of the object
}hazards;
hazards *objList;	// Array of hazards
const float MINSPEED = 0.01;
const float MAXSPEED = 0.09;
const float LOGRADIUS = 0.25;
const float LOGLENGTH = 1.0;

typedef struct 
{
	// Current Position of the frog
	float x, y, z;
	float speed;
	// Position at the start of the jump
	float startX, startZ;

	// X-Y Angle of the frogs direction IN RADIANS
	float theta;
	// X-Z Angle of the frogs direction IN RADIANS
	float phi;

	float yVel;	// Current gravity on frog

	// Sphere variables
	float radius;
	int slices;
	int stacks;
	float normalsLength;

	// Parabola Variables
	float vectorScale;

	// Number of vertices used to produce the frog
	int numVerts;
}Frog;
Frog frog;	 // Instance of the Frog struct

typedef struct
{
	float t;
	float dt;	// Delta Time
	float startTime;
	float lastT;
}Time;
Time timeV;	// Instance of the Time keeping struct

enum Colours { RED, GREEN, BLUE, BROWN, ORANGE, GREY, PINK, YELLOW, WHITE };

// GLfloat light_ambient[] = { 1.0, 0.0, 0.0, 1.0 };
GLfloat red[] = { 1.0, 0.0, 0.0, 0.0 };
GLfloat green[] = { 0.0, 1.0, 0.0, 0.0 };
GLfloat blue[] = { 0.0, 0.0, 1.0, 0.0 };
GLfloat brown[] = { 0.5, 0.25, 0.0, 0.0 };
GLfloat yellow[] = { 1.0, 1.0, 0.0, 0.0 };
GLfloat white[] = { 1.0, 1.0, 1.0, 0.0 };
GLfloat black[] = { 0.0, 0.0, 0.0, 0.0 };
GLfloat grey[] = { 0.25, 0.25, 0.25, 0.0 };

GLuint pictureArray [6];
enum textures { IWOOD, IGRASS, IROAD, IRIVER, ICAR };	// FROG

/* ########################## UTILITY FUNCTIONS ############################ */

void setColour( enum Colours colour )
{
	switch( colour )
	{
		case RED:
			glColor3f( 1, 0, 0 );
			break;
		case GREEN:
			glColor3f( 0, 1, 0 );
			break;
		case BLUE:
			glColor3f( 0, 0, 1 );
			break;
		case BROWN:		// LOGS
			glColor3f( 0.5, 0.25, 0 );
			break;
		case ORANGE:	// CARS
			glColor3f( 1, 0.5, 0 );
			break;
		case GREY:		// ROAD
			glColor3f( 0.25, 0.25, 0.25 );
			break;
		case PINK:		// VELOCITY VECTOR
			glColor3f( 1, 0, 1 );
			break;
		case YELLOW:	// NORMALS + TANGENTS
			glColor3f( 1, 1, 0 );
			break;
		case WHITE:		// FROG
		default:
			glColor3f( 1, 1, 1 );
			break;
	}
}

void drawAxes(float length)
{
	/* DRAWS X AXIS */
	glBegin( GL_LINES );
	glColor3f( 1, 0, 0 );
	glVertex3f( 0, 0, 0 );
	glVertex3f( length, 0, 0 );

	/* DRAWS Y AXIS */
	glColor3f( 0, 1, 0 );
	glVertex3f( 0, 0, 0 );
	glVertex3f( 0, length, 0 );

	/* DRAWS Z AXIS */
	glColor3f( 0, 0, 1 );
	glVertex3f( 0, 0, 0 );
	glVertex3f( 0, 0, length );
	glEnd();
}

/* ########################## BOARD FUNCTIONS ############################ */
void setGrass( )
{
	if ( options.enableTextures )
	glBindTexture(GL_TEXTURE_2D, pictureArray[IGRASS]);
	else 
	{
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, green );
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, green );
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white );
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50 );
	}
	setColour( GREEN );
}

void setRegion( float currZ )
{
	if ( currZ <= ROADSTART )
		setGrass();	
	else if ( currZ <= ROADFINISH )
		{
			if ( options.enableTextures )
				glBindTexture(GL_TEXTURE_2D, pictureArray[IROAD]);
			else 
			{
				glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, grey );
				glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, grey );
				glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white );
				glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50 );
			}
			setColour( GREY );
		}
	else if ( currZ <= RIVERSTART )
		setGrass();
	else if( currZ <= RIVERFINISH )
		{
			if ( options.enableTextures )
				glBindTexture(GL_TEXTURE_2D, pictureArray[IRIVER]);			
			else 
			{
				glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, blue );
				glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, blue );
				glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white );
				glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50 );
			}
			setColour( BLUE );
		}
	else
		setGrass();
}

// Print an axes at the start of each section
void markSections( )
{
	glPushMatrix();

	glTranslatef( board.start, 0, board.start );
	drawAxes(1);

	glTranslatef( 0, 0, 2 );	// to get to -6 (start of road)
	drawAxes(1);

	glTranslatef( 0, 0, 4 );	// to get to -2 (end of road)
	drawAxes(1);

	glTranslatef( 0, 0, 4 );	// to get to 2 (start of river)
	drawAxes(1);

	glTranslatef( 0, 0, 4 );	// to get to 6 (end of river)
	drawAxes(1);

	glPopMatrix();
}

void drawGrid( )
{
	float range = board.finish - board.start;
	float rowStep = range / board.noOfRows;
	float columnStep = range / board.noOfColumns;

	float currZ;

	float textRowStep = 1.0 / board.noOfRows;
	float textColStep = 1.0 / board.noOfColumns;

	if ( options.debugFlag )
		markSections();

	for ( int j = 0; j < board.noOfRows; j++ )
	{
		currZ = ( board.start + (j * rowStep) );
		setRegion( currZ + rowStep );

		// Draw the triangle Stip to build a single row of the grid
		glBegin( GL_TRIANGLE_STRIP );

		/* We must increse the noOfRows here, to compensate for the extra column 
		 * drawn below. Each iteration, we will draw the points 0,1 + 0,0 */
		for ( int i = 0; i < board.noOfColumns + 1; i++ )
		{
			// setRegion( currZ );
			glTexCoord2f( 0, textColStep * i );
			glVertex3f( board.start + ( i * columnStep), 0, currZ );					// B-Left

			// setRegion( currZ );
			glTexCoord2f( 1, textColStep * i);
			glVertex3f( board.start + ( i * columnStep), 0, currZ + rowStep );	// T-Left
		}
		glEnd();
	}

	// Disable texture before normals
	glBindTexture(GL_TEXTURE_2D, 0 );

	if ( options.drawNormals )
	{
		// DRAW NORMALS
		for ( int j = 0; j < board.noOfColumns + 1; j++ )
		{
			// Draw the Normals for the above row
			for ( int i = 0; i < board.noOfRows + 1; i++ )
			{
				glBegin( GL_LINES );
				setColour( YELLOW );
				glNormal3f( 0, 1.0, 0 );
				glVertex3f( board.start + ( i * columnStep), 0,   board.start + (j * rowStep) );
				glVertex3f( board.start + ( i * columnStep), 0.5, board.start + (j * rowStep) );
				glEnd();
			}
		}
	}
}

/* ########################## HAZARD FUNCTIONS ############################ */
void createHazards()
{
	// Generic variable used to store the current range
	float range;

	// Create each object and add them to the objList List
	for ( int i = 0; i < NUMBEROFOBJECTS; i++ )
	{
		if ( i < ( (int) (0.5* NUMBEROFOBJECTS)) )
		{	
			objList[i].objType = LOG;
			// Calculate objects lane
			range = RIVERFINISH - RIVERSTART;
			// Generate between (0 and 1) * Range + Add minimum number required
			objList[i].z = ( (float)rand() )/RAND_MAX * range + RIVERSTART;
		}
		else	
		{			
			objList[i].objType = CAR;
			// Calculate objects lane
			range = ROADFINISH - ROADSTART;
			// Generate between (0 and 1) * Range + Add minimum number required
			objList[i].z = ( (float)rand() )/RAND_MAX * range + ROADSTART;
		}

		// SET THE STARTING COORDINATES
		// Set x coordinate of the object to a value between board.finish and board.start
		range = board.finish - board.start;
		// Generate between (0 and 1) * Range + Add minimum number required
		objList[i].x = ( (float)rand() )/RAND_MAX * range + board.start;

		// SET THE STARTING SPEED
		// Set speed of the object to a value between MAXSPEED and MINSPEED
		range = MAXSPEED - MINSPEED;
		// Generate between (0 and 1) * Range + minimum number required
		objList[i].speed = ( (float)rand() )/RAND_MAX * range + MINSPEED;

		if ( options.debugFlag )
			printf( "Hazards -> %d - x = %f z = %f speed = %f \n", i, objList[i].x, objList[i].z, objList[i].speed );

	}
}

// Individual object movement
float objMove(float x, float speed, float displacement )
{
	x += speed;  //TODO - Perhaps do this in segments (deltas), to ensure that it stops at boundries of screen ##################################

	if ( x >= board.finish )			// x >= LEFTBOUNDARY of Screen
	{
		x = board.start + displacement; // x = RIGHTBOUNDARY  of Screen
	}
	return x;
}

// Control function to manage ALL object movement
void objMoveALL( )
{
	for ( int i = 0; i < NUMBEROFOBJECTS; i++ )
	{
		if ( objList[i].objType == LOG )
			objList[i].x = objMove( objList[i].x, objList[i].speed, LOGLENGTH );
		else 
			objList[i].x = objMove( objList[i].x, objList[i].speed, 1 );
	}
}

void initCar()
{
	float sizeOfCube = 0.25;
	int k = 0;
	
	carVerts = calloc( 8, sizeof(vec3f));
	if (!carVerts) 
	{
		printf( "Failed to allocate carVerts array \n" );
		exit(1);  /* And should print a message! */
	}

	carNormVerts = calloc( 12, sizeof(vec3f));
	if (!carNormVerts) 
	{
		printf( "Failed to allocate carNormVerts array \n" );
		exit(1);  /* And should print a message! */
	}

	float z = 1.0 * sizeOfCube;
	for ( int j = 0; j < 2; j++ )
	{
		carVerts[k].x = -1 * sizeOfCube;
		carVerts[k].y =  1 * sizeOfCube;
		carVerts[k].z = z;
		k++;

		carVerts[k].x =  1 * sizeOfCube;
		carVerts[k].y =  1 * sizeOfCube;
		carVerts[k].z = z;
		k++;

		carVerts[k].x =  1 * sizeOfCube;
		carVerts[k].y = -1 * sizeOfCube;
		carVerts[k].z = z;
		k++;

		carVerts[k].x = -1 * sizeOfCube;
		carVerts[k].y = -1 * sizeOfCube;
		carVerts[k].z = z;
		k++;
		z = -1 * sizeOfCube;
	}

	// Calculate the normal vectors for the cube
	k = 0;
	carNormVerts[k].x =  0;
	carNormVerts[k].y =  0;
	carNormVerts[k].z =  1 * sizeOfCube;
	k++;

	carNormVerts[k].x = -1 * sizeOfCube;
	carNormVerts[k].y =  0;
	carNormVerts[k].z =  0;
	k++;

	carNormVerts[k].x =  0;
	carNormVerts[k].y =  0;
	carNormVerts[k].z = -1 * sizeOfCube;
	k++;

	carNormVerts[k].x =  1 * sizeOfCube;
	carNormVerts[k].y =  0;
	carNormVerts[k].z =  0;
	k++; 

	carNormVerts[k].x =  0;
	carNormVerts[k].y =  1 * sizeOfCube;
	carNormVerts[k].z =  0;
	k++;

	carNormVerts[k].x =  0;
	carNormVerts[k].y = -1 * sizeOfCube;
	carNormVerts[k].z =  0;
	k++;	
}

bool objDrawCar( )
{
	// Bind the texture to be used
	if ( options.enableTextures)
		glBindTexture(GL_TEXTURE_2D, pictureArray[ICAR]);
	else 
	{
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, yellow );
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, yellow );
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white );
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50 );
	}

	glTranslatef( 0, 0.25, 0 );		// Move car above the ground plane
	float nX, nY, nZ;
	
	// Start at 4, due to the 4 points drawn below
	int k = 4;

	// Bind texture to Cube
	glBegin(GL_TRIANGLE_STRIP);
	setColour( YELLOW );
		glNormal3f( 0,1,0);
		glTexCoord2f( 1,1 );
		glVertex3fv((float *) &carVerts[ 1 ] );
		glNormal3f( 0,1,0);
		glTexCoord2f( 0,1 );
		glVertex3fv((float *) &carVerts[ 0 ] );
		glNormal3f( 0,1,0);
		glTexCoord2f( 1,0 );
		glVertex3fv((float *) &carVerts[ 2 ] );
		glNormal3f( 0,1,0);
		glTexCoord2f( 0,0 );
		glVertex3fv((float *) &carVerts[ 3 ] );
	glEnd();

	for ( int j = 1; j < 6; j++ )
	{
		glBegin(GL_TRIANGLE_STRIP);
		setColour( YELLOW );
		for ( int i = 0; i < 4; i++ ) 
		{
			glNormal3f( 0,1,0 );
			glVertex3fv((float *) &carVerts[ cubeVerts[k]] );
			k++;
		}
		glEnd();
	}

	// Disable texture before normals
	glBindTexture(GL_TEXTURE_2D, 0 );

	if ( options.drawNormals )
	{
		glBegin(GL_LINES);
		setColour(YELLOW);
		for ( int i = 0; i < 6; i++ )
		{
			glNormal3fv((float *) &carNormVerts[i] );
			glVertex3fv((float *) &carNormVerts[i] );

			nX = carNormVerts[i].x * frog.normalsLength;
			nY = carNormVerts[i].y * frog.normalsLength;
			nZ = carNormVerts[i].z * frog.normalsLength;
			glVertex3f( nX, nY, nZ );
		}
		glEnd();
	}
}

void initLog()
{
	float r = LOGRADIUS;
	float step_alpha = 2.0 * M_PI / frog.slices;

	/* Allocate arrays */
	logVerts = realloc( logVerts, frog.stacks * sizeof(vec3f) );
	if (!logVerts) 
	{
		printf( "Failed to allocate logVerts array \n" );
		exit(1);  /* And should print a message! */
	}

	// Tube of cylinder
	for ( int j = 0; j < frog.slices; j++ ) 
	{
		logVerts[j].y = r * sinf(step_alpha * j );
		logVerts[j].z = r * cosf(step_alpha * j );
	}
}

bool objDrawLog()
{
	float textStep = 1.0 / frog.slices;
	float iStep = LOGLENGTH / frog.slices;

	// Bind the texture to be used
	if ( options.enableTextures )
		glBindTexture(GL_TEXTURE_2D, pictureArray[IWOOD]);
	else 
	{
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, brown );
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, brown );
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white );
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50 );
	}

	int k = 0;
	// Tube of Cylinder
	for (int j = 0; j < frog.stacks; j++ )
	{
		glBegin( GL_TRIANGLE_STRIP );
		setColour( BROWN );
		for (int i = 0; i <= frog.slices; i++)
		{
			glTexCoord2f( textStep * i, 0 );
			glVertex3f( (-iStep * i), logVerts[j].y, logVerts[j].z );
			glTexCoord2f( textStep * i, 1 );
			if ( j == frog.stacks-1 )
				glVertex3f( (-iStep * i), logVerts[0].y, logVerts[0].z );
			else
				glVertex3f( (-iStep * i), logVerts[j+1].y, logVerts[j+1].z );	
		}
		glEnd();
	}
	
	// Ends of the cylinder
	int x1 = 0;	// LEFT END OF CYLINDER
	for ( int j = 0; j < 2; j++ )
	{
		glBegin(GL_TRIANGLE_STRIP);
		for ( int i = 0; i < frog.slices; i++ )
		{
			glVertex3f( x1, 0, 0 );
			glVertex3f( x1, logVerts[i].y, logVerts[i].z );
		}
		glVertex3f( x1, logVerts[0].y, logVerts[0].z );
		glEnd();
		x1 = -LOGLENGTH; // RIGHT END OF CYLINDER
	}

	// Disable texture before normals
	glBindTexture(GL_TEXTURE_2D, 0 );

	if ( options.drawNormals ) 
	{
		for (int j = 0; j <= frog.stacks; j++ )
		{
			glBegin( GL_LINES );
			setColour( YELLOW );
			for ( int i = 0; i <= frog.slices; i++ )
			{
				glNormal3f( (-iStep * i), logVerts[j].y, logVerts[j].z );
				glVertex3f( (-iStep * i), logVerts[j].y, logVerts[j].z );
				glVertex3f( (-iStep * i), logVerts[j].y * frog.normalsLength, logVerts[j].z * frog.normalsLength );
			}
			glEnd();
		}
	}
}

// Control function to Display ALL objects to screen
void objDisplayAll()
{
	bool collisionDetected = false; 

	for ( int i = 0; i < NUMBEROFOBJECTS; i++ )
	{
		glPushMatrix();
		glTranslatef(objList[i].x, 0, objList[i].z);

		if ( objList[i].objType == CAR )  
		{
			if ( objDrawCar( ) )
				collisionDetected = true;
		}
		else
		{
			if ( objDrawLog( ) )
				collisionDetected = true;
		}  

		if ( options.drawAxes )
			drawAxes(1);

		glPopMatrix();
	}
	//return collisionDetected;			//TODO IN NEXT ASSIGNMENT	######################################################################
}

/* ########################## PROJECTILE FUNCTIONS ######################### */
void initSphere()
{
	// Temp variables to allow shorter code
	int stacks = frog.stacks;
	int slices = frog.slices;	
	float length = frog.normalsLength;

	float r = frog.radius;

	float alpha;
	float beta;

	int k = 0;

	// numVerts =  (stacks - ENDS) *  (slices) + 2 end points
	// NOTE +1's are for the <= conditions
	frog.numVerts = (stacks - 2 + 1) * (slices + 1) + 2;

	/* Allocate arrays */
	frogVerts = realloc( frogVerts, frog.numVerts * sizeof(vec3f) );
	if (!frogVerts) 
	{
		printf( "Failed to allocate frogVerts array \n" );
		exit(1);  /* And should print a message! */
	}

	frogVerts[k].x = 0;
	frogVerts[k].y = r;	// The vertex at the top of the sphere where x and z = 0
	frogVerts[k].z = 0;
	k++;

	for (int j = 1; j <= stacks-1; j++) 
	{
		beta = j / (float)stacks * M_PI;
		for (int i = 0; i <= slices; i++)
		{
			alpha = i / (float)slices * 2.0 * M_PI;
			frogVerts[k].x = r * sinf(beta) * cosf(alpha);
			frogVerts[k].y = r * cosf(beta);
			frogVerts[k].z = r * sinf(beta) * sinf(alpha);
			k++;
		}		
	}

	frogVerts[k].x = 0;
	frogVerts[k].y = -r;	// The vertex at the bottom of the sphere where x and z = 0
	frogVerts[k].z = 0;
	k++;
}

void drawSphere()
{
	// Bind the texture to be used
	if ( !options.enableTextures)
	{
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, red );
		glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, red );
		glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, white );
		glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 50 );
	}

	int slices = frog.slices;
	int stacks = frog.stacks;
	float length = frog.normalsLength;

	float nX, nY, nZ;

	int k = 1;
	// Top of sphere
	glBegin(GL_TRIANGLE_STRIP);
	setColour( RED );
	for (int i = 0; i <= slices; i++)
	{	
		glVertex3fv((float *) &frogVerts[0] );
		glVertex3fv((float *) &frogVerts[k] );
		k++;
	}
	glEnd();

	// Middle of Sphere
	k = 1;
	for (int j = 1; j <= stacks-2; j++)
	{
		glBegin(GL_TRIANGLE_STRIP);
		for (int i = 0; i <= slices; i++)
		{
			glVertex3fv((float *) &frogVerts[ k ] );
			glVertex3fv((float *) &frogVerts[ k+slices+1 ] );
			k++;
		}
		glEnd();
	}

	// Bottom of sphere
	glBegin(GL_TRIANGLE_STRIP);
	for (int i = 0; i <= slices; i++)
	{	
		glVertex3fv((float *) &frogVerts[ frog.numVerts - 1 ] );
		glVertex3fv((float *) &frogVerts[k] );
		k++;
	}
	glEnd();


	if ( options.drawNormals )
	{
		glBegin(GL_LINES);
		setColour( YELLOW );
		for (int i = 0; i < frog.numVerts; i++) 
		{
			nX = frogVerts[ i ].x * frog.normalsLength;
			nY = frogVerts[ i ].y * frog.normalsLength;
			nZ = frogVerts[ i ].z * frog.normalsLength;

			glNormal3fv((float *) &frogVerts[ i ] );
			glVertex3fv((float *) &frogVerts[ i ] );
			glVertex3f( nX, nY, nZ );
		}
		glEnd();
	}
}

void stopFrog()
{
	options.start = false;
	frog.y = 0;
	frog.startX = frog.x;
	frog.startZ = frog.z;
	frog.yVel = frog.speed * sin(frog.theta);

	timeV.t = 0;
	timeV.dt = 0;
	timeV.lastT = -1;
}

void drawVelocityVector()
{
	float vectorScale = 1;

	float targetx = frog.speed * frog.vectorScale * cos(frog.theta) * cos(frog.phi);
	float targety = frog.speed * frog.vectorScale * sin(frog.theta);
	float targetz = frog.speed * frog.vectorScale * cos(frog.theta) * sin(frog.phi);

	glRotatef( M_PI, 0, 1, 0 );
	glBegin(GL_LINES);
	setColour( PINK );

	glVertex3f( 0, 0, 0);
	glVertex3f( targetx, targety, targetz );
	glEnd();

	if ( options.debugFlag )
		printf( "x = %f, y = %f, z = %f \n", targetx, targety, targetz );
}

void drawParametricParabola()
{
	double dt;
	double flightTime = (2 * frog.speed * sin(frog.theta) ) / GRAVITY;
	double thetaTime = flightTime / frog.slices;
	double x, y, z;
	double nX, nY, nZ;
	int i;
	
	glBegin( GL_LINE_STRIP );
	setColour( WHITE );
	for( i = 0; i <= frog.slices; i++)
	{
		dt = (thetaTime * i);

		x = frog.speed * dt * cos(frog.theta) * cos(frog.phi);
		y = frog.speed * dt * sin(frog.theta) - (0.5 * GRAVITY * dt * dt );
		z = frog.speed * dt * cos(frog.theta) * sin(frog.phi);

		glVertex3f( x, y, z );
	}
	glEnd();


	// If tangents or normals need to be drawn 
	if ( options.drawNormals )
	{
		glBegin( GL_LINES );
		setColour( YELLOW );
		for( i = 0; i <= frog.slices; i++)
		{
			dt = (thetaTime * i);

			x = frog.speed * dt * cos(frog.theta) * cos(frog.phi);
			y = frog.speed * dt * sin(frog.theta) - (0.5 * GRAVITY * dt * dt );
			z = frog.speed * dt * cos(frog.theta) * sin(frog.phi);

			nX = frog.speed * cos(frog.theta) * cos(frog.phi);
			nY = frog.speed * sin(frog.theta) - ( GRAVITY * dt );
			nZ = frog.speed * cos(frog.theta) * sin(frog.phi);
	
			// Normalize the vector 
			double magnitude = sqrt(( nX * nX ) + ( nY * nY ) + ( nZ * nZ ));
			nX = ( nX / magnitude ); 
			nY = ( nY / magnitude );
			nZ = ( nZ / magnitude );

			// Scale the vector 
			nX = ( nX * frog.vectorScale ) + x;
			nY = ( nY * frog.vectorScale ) + y;
			nZ = ( nZ * frog.vectorScale ) + z;

			glVertex3f( x, y, z );
			glVertex3f( nX, nY, nZ );
		}
		glEnd();
	}	
}

void calcPosition()	
{
	if ( frog.y < 0 )
	{
		stopFrog();
		return;
	}
	else
	{
		// Position
		frog.x += frog.speed * cos(frog.theta) * cos(frog.phi) * timeV.dt;
		frog.y += frog.yVel  * timeV.dt;
		frog.z += frog.speed * cos(frog.theta) * sin(frog.phi) * timeV.dt;

		// Velocity
		frog.yVel += -GRAVITY * timeV.dt;		
	}
}

/* ########################## CAMERA FUNCTIONS ############################ */
void mouseMotion(int x, int y)
{
	float zoomScale = 100.0;

	/* Generic camera set variables */
	camera.dX = x - camera.lastX;
	camera.dY = y - camera.lastY;
	camera.lastX = x;
	camera.lastY = y;

 	/* Camera Movement */
	if ( camera.rotate )
	{
		camera.xRotation += camera.dX;
		camera.yRotation += camera.dY;
	}

	/* Camera Scaling - Using dX for both variables to ensure smooth scaling  */
	if ( camera.zoom )
	{
		camera.zoomFactorX += ( camera.dX / zoomScale );
		camera.zoomFactorY += ( camera.dX / zoomScale );
	}
}

void mouseBtnClick( int x, int y )
{
	camera.lastX = x;
	camera.lastY = y;
}

void mouseEvent(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN && !camera.zoom) 
	{
		camera.rotate = true;
		mouseBtnClick( x, y );
	}
	if (button == GLUT_LEFT_BUTTON && state == GLUT_UP && !camera.zoom)  
	{
		camera.rotate = false;
	}

	if (button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN && !camera.rotate ) 
	{
		mouseBtnClick( x, y );
		camera.zoom = true;
	}
	if (button == GLUT_RIGHT_BUTTON && state == GLUT_UP && !camera.rotate ) 
	{
		camera.zoom = false;
	}
}

void cameraMovements() 
{
	if ( options.interactive )
	{	
		/* Focus on the frog + Zoom in and out of the screen */
		gluLookAt( frog.x, 1, frog.z - camera.zoomFactorX - 4,
					frog.x, 0, frog.z,
					0, 1.0, 0 );
	}
	else // Interactive controls using NumPad and Mouse Zoom
	{
		glTranslatef( camera.xPos, camera.yPos, camera.zoomFactorX - 5 );
	}

	// Rotate based upon x-Mouse Movements
	glRotatef( camera.xRotation, 0.0, 1.0, 0.0 );
	// Rotate based upon y-Mouse Movements
	glRotatef( camera.yRotation, 1.0, 0.0, 0.0 );

	/* Draw the GLOBAL axes to the screen */
	if (options.drawAxes)
		drawAxes(5);
}

/* ########################## TEXTURE AND LIGHTING FUNCTIONS ############################ */

static GLuint loadTexture(const char *filename)
{
	GLuint tex = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);
	if (!tex)
		return 0;

	glBindTexture(GL_TEXTURE_2D, tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glBindTexture(GL_TEXTURE_2D, 0);

	return tex;
}

void lightingFunctions()
{
	GLfloat position [] = {1,1,1,0};

   glEnable(GL_LIGHTING);
   glEnable(GL_LIGHT0);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, white );
   glLightfv(GL_LIGHT0, GL_AMBIENT, white );
   glLightfv(GL_LIGHT0, GL_SPECULAR, white );

   glLightfv(GL_LIGHT0, GL_POSITION, position );
}

void disableLighting()
{
	glDisable(GL_LIGHTING);
	glDisable(GL_LIGHT0);
}

/* ########################## SYSTEM FUNCTIONS ############################ */

void display()
{
	GLenum err;		/* Used to contain any error codes found */

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	if ( options.enableTextures )
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else 
		glDisable(GL_BLEND);

	/* Draw the world axes at 0,0 with original orientation to the screen */
	if (options.drawAxes)
		drawAxes(1);

	if ( options.enableTextures )
		glEnable(GL_TEXTURE_2D);
	else 
		glDisable(GL_TEXTURE_2D);

	if ( options.enableLighting )
		lightingFunctions();
	else
		disableLighting();

	if ( options.wireFrame )
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE );
	else
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL );

	cameraMovements();

	// Draw the ground plane
	drawGrid();

	// Display the objects to the screen 
	objDisplayAll();

	glPushMatrix();
		// Move the frog to its current position
		glTranslatef( frog.x, frog.y + frog.radius, frog.z );
		drawSphere();
	glPopMatrix();

	glPushMatrix();
		// Move the vector and parabola to the frogs location
		glTranslatef( frog.startX, frog.radius, frog.startZ);
		drawVelocityVector();
		drawParametricParabola();
	glPopMatrix();

	glutSwapBuffers();

	// Check for errors		
	while ((err = glGetError()) != GL_NO_ERROR)
	printf("%s\n",gluErrorString(err));
}

void idle()
{
	// Animate the objects
	objMoveALL();

	if ( options.start ) 
	{
		timeV.t = glutGet(GLUT_ELAPSED_TIME) / 1000.0 - timeV.startTime;

		if (timeV.lastT < 0.0) 
		{
			timeV.lastT = timeV.t;
			return;
		}

		timeV.dt = timeV.t - timeV.lastT;
		if ( options.debugFlag )
			printf(" idle()  t = %f, dt = %f\n", timeV.t, timeV.dt);
		
		calcPosition( );

		timeV.lastT = timeV.t;
	}
   
	glutPostRedisplay();	
}

void reshape (int width, int height) 
{
	glViewport(0, 0, width, height);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();			
	gluPerspective( 75, ((float) width / (float) height), 0.1, 1000 ); 
	
	if ( options.debugFlag )
		printf( "Reshape - width = %d, height = %d  \n", width, height);
}

// Calculate the arrays of points for each of the following objects
void reInitializeObjects()
{
	initSphere();
	initLog();
	initCar();
}

void init()
{
	// Seed the random number Generator
	time_t t;	
	srand((unsigned) time(&t));

	glEnable(GL_NORMALIZE);

	camera.zoomFactorX = -2;
	// camera.zoomFactorY = 1;

	camera.xRotation = 0;
	camera.yRotation = 0;

	frog.x = 0;
	frog.y = 0;
	frog.z = board.start + frog.radius;
	frog.speed = 3;
	frog.theta = M_PI/4;
	frog.phi = M_PI/2;
	frog.radius = 0.25;
	frog.slices = 8;
	frog.stacks = 8;
	frog.normalsLength = 1.25;
	frog.vectorScale = 0.25;

	frog.startX = board.start / 2;
	frog.startZ = board.start + (2 * frog.radius);

	camera.yPos = -2;
	options.interactive = true;

	stopFrog();

	// Load the textures
	pictureArray [IWOOD]  = loadTexture( "wood.jpg" );
	pictureArray [IGRASS] = loadTexture( "grass.jpg" );
	pictureArray [IROAD]  = loadTexture( "road.jpg" );
	pictureArray [IRIVER] = loadTexture( "river.jpg" );
	pictureArray [ICAR]   = loadTexture( "car.jpg");

	/* TESTING TEXTURE PATTERNS - MUST LOAD THESE TEXTURES BEFORE TEXTURING
	 * ANY OBJECTS, OTHERWISE THIS OPERATION WILL NOT CHANGE ANYTHING
	 * HENCE WHY THIS MUST BE UNCOMMENTED TO USE. A DEBUG FLAG WILL NOT WORK
	 */
	// for ( int i = 0; i < sizeof(pictureArray)/sizeof(pictureArray[0]); i++ )
	// {
	// 	pictureArray [i] = loadTexture( "GRID.jpg" );
	// }

	// Array to store all of the collision objects
	objList = calloc(NUMBEROFOBJECTS, sizeof(hazards));
	reInitializeObjects();
	createHazards();
}

void quit()
{
	free(objList);
	free(logVerts);
	free(carVerts);
	free(carNormVerts);
	free(frogVerts);
	printf(" ### THANKS FOR PLAYING ### \n");
	exit(EXIT_SUCCESS);
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
		case 'o':
			options.drawAxes = !options.drawAxes;
			break;
		case 'n':
			options.drawNormals = !options.drawNormals;
			break;
		case 'p':
			options.wireFrame = !options.wireFrame;
			break;
		case 'l':
			if ( options.enableLighting )
				options.enableLighting = false;
			else
			{
				options.enableLighting = true;
				options.enableTextures = false;
			}
			break;
		case 't':
			if ( options.enableTextures )
				options.enableTextures = false;
			else 
			{
				options.enableTextures = true;
				options.enableLighting = false;
			}
			break;
		case 'w':
			frog.speed += 0.1;
			break;
		case 's':
			if ( frog.speed > 0.1 )
				frog.speed -= 0.1;
			break;
		case 'a':
			if ( frog.theta < M_PI/2 )
				frog.theta += ONERADIAN;
			break;
		case 'd':
			if ( frog.theta > 0 )
				frog.theta -= ONERADIAN;
			break;
		case '+':		
			board.noOfRows = board.noOfRows * 2;
			board.noOfColumns = board.noOfColumns * 2;
			frog.slices = frog.slices * 2;
			frog.stacks = frog.stacks * 2;
			reInitializeObjects();
			break;
		/* NOTE I have used the number of rows on the board as the stop variable
		 * 	  as any less than 4 rows, and the sections (road, river etc) will
		 *		  NOT be display correctly as there are 4 sections which will need
		 *      AT least a row to display each section correctly */
		case '-':		
			if ( board.noOfRows > 4 )
			{
				board.noOfRows = board.noOfRows / 2;
				board.noOfColumns = board.noOfColumns / 2;
				frog.slices = frog.slices / 2;
				frog.stacks = frog.stacks / 2;
				reInitializeObjects();
			}
			break;
		case 'q':
			quit();
			break;
		case 'r':	/* Reset the frog back to 0, 0, start  */
			frog.x = 0.0;
			frog.z = board.start + frog.radius;
			stopFrog();
			break;
		case ' ':	/* Spacebar Key */
			if ( !options.start ) 
			{				
				timeV.startTime = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
				options.start = true;
			}
		break;


		// #### EXTRA CONTROLS ####

		// Activate the debugging outputs 
		case 'z':	
			options.debugFlag = !options.debugFlag;
			break;

		// INTERACTIVE CAMERA CONTROLS
		// Left and Right on the X Axis
		case '4':
			camera.xPos += 0.1;
			break;
		case '6':
			camera.xPos -= 0.1;
			break;
		// Up and down on Y axis
		case '8':
			camera.yPos -= 0.1;
			break;
		case '2':
			camera.yPos += 0.1;
			break;
		case 'y':	// Switch between interactive and look at camera's	
			options.interactive = !options.interactive;
			break;
		default:
			break;
	}
}

void keyboardSpecial(int key, int x, int y)
{
	switch (key)
	{
		case GLUT_KEY_LEFT:
		{
			frog.phi -= ONERADIAN;
			break;			
		}
		case GLUT_KEY_RIGHT:
		{
			frog.phi += ONERADIAN;
			break;
		}
	}
}

int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize (700, 700); 
	glutInitWindowPosition (100, 100);
	glutCreateWindow("Assignment 2");

	init();

	// Callbacks
	glutDisplayFunc( display );
	glutMouseFunc( mouseEvent );
	glutMotionFunc( mouseMotion );
	glutKeyboardFunc( keyboard );
	glutSpecialFunc( keyboardSpecial );
	glutReshapeFunc ( reshape );

	glutIdleFunc ( idle );

	glutMainLoop();

	return EXIT_SUCCESS;
}